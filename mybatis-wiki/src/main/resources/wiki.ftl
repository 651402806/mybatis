h3.描述
|描述
|描述:${templateMap.interfaceComment}

h3.接口基本信息:
|*接口基本信息*|  |
|接口名称|  |
|HTTP请求方式|HTTP-POST|
|接口地址|测试:
开发:|

h3.平台组部署依据:
|平台组部署依据|   |
|服务定义|值|
|服务名称|${templateMap.qualifiedName}|
<#list templateMap.methodList as methods>
<#list methods.parameterTypeList as parameterType>
|入参类名|${parameterType}|
</#list>
|出参类名|${methods.qualifiedRspName}|
|方法名称|${methods.methodName}|
</#list>

h3.参数说明
<#list templateMap.methodList as methods>
<#list methods.parameterTypeList as parameterType>
h5.入参(${parameterType})
|*字段名称*|*说明*|*类型*|*必填*|*默认值*|*长度*|
<#list variableMap[parameterType] as variable>
|${variable.variableName}| ${variable.comment} |${variable.type}| |   |   |
</#list>
</#list>

h5.出参(${methods.qualifiedRspName})
|*字段名称*|*说明*|*类型*|
<#assign rspType=methods.qualifiedRspName/>
<#list variableMap[rspType] as variable>
|${variable.variableName}| ${variable.comment} |${variable.type}|
</#list>

<#list variableTypeSet as elementSet>
${elementSet}
|*字段名称*|*说明*|*类型*|
<#list variableMap[elementSet] as variable>
|${variable.variableName}| ${variable.comment} |${variable.type}|
</#list>

</#list>
</#list>

