package com.changyu.zhy.config;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述：模板配置类
 *
 * @author zhanghaoyu
 * @date 2018/12/17 9:50
 **/
public class TemplateConfig {
    public static Template config(){
        Template template=null;
        try {
            //配置jar包版本
            Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
            //设定模板异常处理器
            configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            //设定模板处理编码
            configuration.setDefaultEncoding("UTF-8");
            //设定从当前类的类加载路径的根目录classes下查找模板文件
            configuration.setClassForTemplateLoading(TemplateConfig.class, "/");
            //指定读取的模板文件名
            template = configuration.getTemplate("wiki.ftl");
            //配置模板使用的map
           /* for (int i=0;i<3;i++) {
                Map<String, Object> templateMap = new HashMap<>(16);
                FileWriter fileWriter = new FileWriter("D:/gitlab-repository/mybatis/mybatis-wiki/src/main/java/com/changyu/zhy/Template/template"+i+".txt");
                template.process(templateMap,fileWriter);
                fileWriter.flush();
                fileWriter.close();
            }*/
        } catch (Exception templateException) {
            templateException.printStackTrace();
        }
        return template;
    }
}
