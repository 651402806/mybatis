package com.changyu.zhy.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * 描述：处理properties配置文件
 *
 * @author zhanghaoyu
 * @date 2018/12/18 19:09
 **/
public class PropertiesHandler {
    /**
     * 黑名单，使用set集合可以去重
     */
    private Set<String> blackSet;
    /**
     * 模块路径
     */
    private String moubldeName;
    /**
     * wiki标记生成路径
     */
    private String wikiPath;
    /**
     * 从配置文件读取的blackSetStr
     */
    private String blackSetStr;

    /**
     * wiki配置文件路径
     */
    private static final String PROPERTIES_FILE="/wiki-config.properties";

    public PropertiesHandler() {
        blackSet=new HashSet<>(16);
        blackSet.add("java.*");
        blackSet.add("freemarker.*");
    }

    /**
     * 在类路径下的resource读取指定的配置文件wiki-cofig.properties
     */
    public Properties readProperties(){
        FileInputStream fileInputStream=null;
        Properties properties=null;
        try {
//            ClassLoader classLoader = PropertiesHandler.class.getClassLoader();
//            URL resource = classLoader.getResource(PROPERTIES_FILE);
//            String path = resource.getPath();
            //TODO 不存在此配置文件时的处理
            properties = new Properties();
//            fileInputStream = new FileInputStream(path);
            properties.load(PropertiesHandler.class.getResourceAsStream(PROPERTIES_FILE));
//            packageParam(properties);
        } catch (IOException e) {
            e.printStackTrace();
        } /*finally {
            try {
//                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
        return properties;
    }

    /**
     * 组装配置文件中的数据
     * @param properties
     */
    public void packageProperties(Properties properties){
        this.moubldeName=properties.getProperty("module.name");
        this.wikiPath=properties.getProperty("wiki.path");
        this.blackSetStr=properties.getProperty("black.set");
        //加入配置文件中的黑名单
        String[] split = blackSetStr.split(",");
        for (String str : split) {
            blackSet.add(str);
        }
    }

    public Set<String> getBlackSet() {
        return blackSet;
    }

    public void setBlackSet(Set<String> blackSet) {
        this.blackSet = blackSet;
    }

    public String getMoubldeName() {
        return moubldeName;
    }

    public void setMoubldeName(String moubldeName) {
        this.moubldeName = moubldeName;
    }

    public String getWikiPath() {
        return wikiPath;
    }

    public void setWikiPath(String wikiPath) {
        this.wikiPath = wikiPath;
    }

    public String getBlackSetStr() {
        return blackSetStr;
    }

    public void setBlackSetStr(String blackSetStr) {
        this.blackSetStr = blackSetStr;
    }
}
