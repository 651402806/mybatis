package com.changyu.zhy.api;

import com.sun.javadoc.RootDoc;

import java.util.Map;

/**
 * 描述：组装各个doc中的所有信息
 *
 * @author zhanghaoyu
 * @date 2018/12/13 15:47
 **/
public interface PackageDocs {
    /**
     * 返回包装好的doc信息
     *
     * @param rootDoc
     * @return void
     */
    void packageDoc(RootDoc rootDoc);
}
