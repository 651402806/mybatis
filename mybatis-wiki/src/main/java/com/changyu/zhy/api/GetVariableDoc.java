package com.changyu.zhy.api;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.FieldDoc;

/**
 * 描述：获取类的变量
 *
 * @author zhanghaoyu
 * @date 2018/12/13 15:28
 **/
public interface GetVariableDoc {
    /**
     *
     * @return 获取类中的fieldDoc
     */
    FieldDoc[] getVariables(ClassDoc classDoc);
}
