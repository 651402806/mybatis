package com.changyu.zhy.api;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.MethodDoc;

/**
 * 描述：获取类中的方法
 *
 * @author zhanghaoyu
 * @date 2018/12/13 15:23
 **/
public interface GetMethodDoc {
    /**
     *
     * @return 类中的所有方法doc树
     */
    MethodDoc[] getMethods(ClassDoc classDoc);
}
