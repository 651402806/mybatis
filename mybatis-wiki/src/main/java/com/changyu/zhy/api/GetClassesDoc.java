package com.changyu.zhy.api;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.RootDoc;

/**
 * 描述：获取类文件Doc接口
 *
 * @author zhanghaoyu
 * @date 2018/12/13 15:17
 **/
public interface GetClassesDoc {
    /**
     *
     * @return 与类有关的Doc树
     */
    ClassDoc[] getClasses(RootDoc rootDoc);
}
