package com.changyu.zhy.api;

import freemarker.template.Template;

import java.util.Map;

/**
 * 描述：输出文件
 *
 * @author zhanghaoyu
 * @date 2018/12/17 14:33
 **/
public interface FileOutput {
    /**
     * 输出文件
     * @param template
     * @param rootMap 根数据模型
     */
    void output(Template template, Map rootMap,String serviceName,String wikiPath);

    /**
     * 输出命令临时文件
     */
    String outputTemporary(String moduleName);

    /**
     * 删除命令临时文件
     */
    void deleteTemporary(String tempPath);

}
