package com.changyu.zhy.api;

import java.io.File;
import java.util.List;

/**
 * 描述：读取文件夹下的所有java文件
 *
 * @author zhanghaoyu
 * @date 2018/12/20 16:36
 **/
public interface ReadFilePath {
    /**
     * 读取指定文件夹下的java文件列表
     * @param rootPath
     * @return
     */
    File[] readJavaFilePath(String rootPath);

    /**
     * 读取指定路径下的文件及目录
     * @param file
     * @param files 包含所有java文件的数组
     * @return
     */
    void readFile(File file, List<File> files);

    /**
     * 将路径下的所有文件拼接成一个字符串
     * @param files
     * @return
     */
    String concatFilePath(File[] files);

    /**
     * 拼接javadoc使用的命令参数
     * @param elementStr
     * @return
     */
    String concatCommandLine(String...elementStr);
}
