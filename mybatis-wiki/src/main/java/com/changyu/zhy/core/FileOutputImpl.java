package com.changyu.zhy.core;

import com.changyu.zhy.api.FileOutput;
import com.changyu.zhy.api.ReadFilePath;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 描述：输出文件
 *
 * @author zhanghaoyu
 * @date 2018/12/17 14:32
 **/
public class FileOutputImpl implements FileOutput {
    /**
     * 按模板文件输出
     * @param template 模板句柄
     * @param rootMap 根数据模型
     * @param serviceName 服务名
     */
    @Override
    public void output(Template template, Map rootMap,String serviceName,String wikiPath) {
        try {
//            FileWriter fileWriter = new FileWriter("D:/gitlab-repository/mybatis/mybatis-wiki/src/main/java/com/changyu/zhy/template/"+serviceName+".txt");
            File file = new File(wikiPath + "/" + serviceName + ".txt");
            if(!file.getParentFile().exists()){
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(wikiPath+"/"+serviceName+".txt");
            template.process(rootMap,fileWriter);
            fileWriter.flush();
            fileWriter.close();
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 输出一个装载了jar包路径和java文件路径的临时文件
     *
     * @return 返回文件生成路径
     */
    @Override
    public String outputTemporary(String modulePath){
        //获取项目所在根路径，不包含模块路径
        String projectPath = System.getProperty("user.dir");
        /*String modulePath = projectPath.concat("/"+moduleName);*/
        //TODO 删除测试
//        modulePath="D:\\ytxk\\teleOrder\\teleOrder-api";
        ReadFilePath readFilePath = new ReadFilePathImpl();
        File[] files = readFilePath.readJavaFilePath(modulePath);
        String concatFilePath = readFilePath.concatFilePath(files);
        String jarPath = "\""+System.getProperty("java.class.path")+"\"";
        String concatCommandLine = readFilePath.concatCommandLine("-classpath", jarPath, concatFilePath, "-verbose");
        File file = new File(projectPath+"/temp");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
            outputStreamWriter.write(concatCommandLine.replaceAll("\\\\","/"));
            outputStreamWriter.flush();
            outputStreamWriter.close();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }

    /**
     * 删除命令临时文件
     * @param tempPath
     */
    @Override
    public void deleteTemporary(String tempPath) {
        File file = new File(tempPath);
        boolean delete = file.delete();
    }
}
