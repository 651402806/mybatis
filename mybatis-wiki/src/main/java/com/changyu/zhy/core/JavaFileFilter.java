package com.changyu.zhy.core;

import java.io.File;
import java.io.FileFilter;

/**
 * 描述：Java文件过滤器，只保留java文件和目录
 *
 * @author zhanghaoyu
 * @date 2018/12/20 17:48
 **/
public class JavaFileFilter implements FileFilter {
    @Override
    public boolean accept(File file) {
        if(file.isDirectory()){
            return true;
        }else{
            return file.getName().endsWith(".java");
        }
    }
}
