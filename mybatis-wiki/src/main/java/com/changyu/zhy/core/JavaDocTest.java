/*
package com.changyu.zhy.core;*/
/*
import com.sun.javadoc.*;
*//*

*/
/**
 * 描述：
 * @param com.changyu.zhy.core.JavaDocTest
 * @author zhanghaoyu
 * @date 2018/12/12 16:09
 **//*
*/
/*

public class com.changyu.zhy.core.JavaDocTest extends Doclet {
    public static boolean start(RootDoc root) {
        ClassDoc[] classes = root.classes();
        for (int i = 0; i < classes.length; ++i) {
            ClassDoc cd = classes[i];
            printMembers(cd.constructors());
            printMembers(cd.methods());
        }
        return true;
    }

    static void printMembers(ExecutableMemberDoc[] mems) {
        for (int i = 0; i < mems.length; ++i) {
            ParamTag[] params = mems[i].paramTags();
            System.out.println(mems[i].qualifiedName());
            for (int j = 0; j < params.length; ++j) {
                System.out.println(" s  " + params[j].parameterName()
                        + " - " + params[j].parameterComment());
            }
        }
    }

    public static void main(String[] args) {
        com.sun.tools.javadoc.Main.execute(new String[]{
                "-doclet",Doclet.class.getName(),
                "-encoding","utf-8", "-classpath","D:\\gitlab-repository\\mybatis\\mybatis-service\\target\\mybatis-service-1.0.0-SNAPSHOT\\WEB-INF\\lib\\fastjson-1.2.47.jar",
                "D:\\gitlab-repository\\mybatis\\mybatis-core\\src\\main\\java\\com.changyu.zhy\\aop\\AopCommonAspect.java"
        });

    }
}
*//*

import com.sun.javadoc.*;
import com.sun.tools.javadoc.FieldDocImpl;
import org.slf4j.LoggerFactory;

import java.security.CodeSource;
import java.security.ProtectionDomain;

*/
/**
 * JavaDoc的使用方法
 *//*

public class JavaDocTest {
    private static RootDoc rootDoc;
    public static  class Doclet {
        public static boolean start(RootDoc rootDoc) {
            JavaDocTest.rootDoc = rootDoc;
            show();
            return true;
        }
    }

    */
/**
     * 显示DocRoot中的基本信息
     *//*

    public static void show(){
        ClassDoc[] classes = rootDoc.classes();
        for(ClassDoc classDoc : classes){
            System.out.println(classDoc.name()+
                    "类的注释:"+classDoc.commentText());
            MethodDoc[] methodDocs = classDoc.methods();
            FieldDoc[] fields = classDoc.fields();
            String s = fields[1].constantValueExpression();
            Object o = fields[1].constantValue();
            SerialFieldTag[] serialFieldTags = fields[1].serialFieldTags();
            Type type = fields[1].type();
            TypeVariable[] typeVariables = classDoc.typeParameters();
            ParamTag[] paramTags = classDoc.typeParamTags();
            FieldDoc[] fields1 = classDoc.fields(false);
            String s1 = fields1[1].commentText();
            String s2 = fields1[2].commentText();
            boolean b = classDoc.definesSerializableFields();
            FieldDoc[] fieldDocs = classDoc.serializableFields();
            for(MethodDoc methodDoc : methodDocs){
                // 打印出方法上的注释
                System.out.println("类"
                        +classDoc.name()+","
                        +methodDoc.name()+
                        "方法注释:"
                        +methodDoc.commentText());
            }
        }
    }

    public static void main(String[] args) {
        ProtectionDomain protectionDomain = LoggerFactory.class.getProtectionDomain();
        CodeSource codeSource = protectionDomain.getCodeSource();
        System.out.println("路径:"+codeSource.getLocation());
        String filePath = System.getProperty("java.class.path");
        System.out.println("i am jar path\n"+filePath+"\nEnd\n");
        //注意命令顺序
        com.sun.tools.javadoc.Main.execute(JavaDocTest.class.getClassLoader(),new String[] {"-private","-doclet",
                Doclet.class.getName(),
                "-encoding","utf-8","-classpath",
                "D:\\Java\\jdk1.8.0_144\\lib\\tools.jar","-sourcepath",
                "D:\\gitlab-repository\\mybatis\\mybatis-api\\src\\main\\java","-subpackages","com.changyu.zhy","-verbose"});

        */
/*com.sun.tools.javadoc.Main.execute(JavaDocTest.class.getClassLoader(),new String[] {"-protected","-splitindex",
                "-encoding","utf-8","-d",
                "H:\\javadocTest",
                "@C:\\Users\\zhy\\Desktop\\jarpath"});*//*

        //javadoc -protected -splitindex -encoding utf-8 -d H:\javadocTest @C:\Users\zhy\Desktop\jarpath
    }
}
*/
