package com.changyu.zhy.core;

import com.changyu.zhy.api.GetMethodDoc;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.MethodDoc;

/**
 * 描述：获取扫描类文件中的方法
 *
 * @author zhanghaoyu
 * @date 2018/12/13 15:10
 **/
public class GetMethodDocImpl implements GetMethodDoc {
    @Override
    public MethodDoc[] getMethods(ClassDoc classDoc) {
        MethodDoc[] methods = classDoc.methods();
        return methods;
    }
}
