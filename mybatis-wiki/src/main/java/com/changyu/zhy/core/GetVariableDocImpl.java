package com.changyu.zhy.core;

import com.changyu.zhy.api.GetVariableDoc;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.FieldDoc;

/**
 * 描述：获取扫描类原文件中的变量注释
 *
 * @author zhanghaoyu
 * @date 2018/12/13 15:08
 **/
public class GetVariableDocImpl implements GetVariableDoc {
    @Override
    public FieldDoc[] getVariables(ClassDoc classDoc) {
        FieldDoc[] fields = classDoc.fields();
        return fields;
    }
}
