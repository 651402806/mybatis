package com.changyu.zhy.core;

import com.changyu.zhy.api.*;
import com.changyu.zhy.config.PropertiesHandler;
import com.changyu.zhy.config.TemplateConfig;
import com.changyu.zhy.constant.BaseDateTypeHandler;
import com.changyu.zhy.util.MatchUtil;
import com.sun.javadoc.*;
import freemarker.template.Template;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 描述：组装doc中的所有信息
 *
 * @author zhanghaoyu
 * @date 2018/12/13 15:46
 **/
public class PackageDocsImpl implements PackageDocs {
    /**
     * 存储扫描某个类的时候得到的变量类型全类名,使用set去重,并且set按插入顺序排列
     */
    private static List<String> variableTypeSet=new ArrayList<>();
    /**
     * 记录某一个全类名属于哪一个classDoc
     */
    private static Map<String,Object> mapperToClassDoc=new LinkedHashMap<>();
    private static HashMap<String, Object> root = new HashMap<>(16);
    private static Map<String, Object> templateMap = new HashMap<>(16);
    private static Map<String, Object> variableMap = new HashMap<>(16);

    private GetClassesDoc classesDoc;
    private GetMethodDoc methodDoc;
    private GetVariableDoc variableDoc;
    private FileOutput fileOutput;
    private PropertiesHandler propertiesHandler;

/*    public void init() {
        classesDoc = new GetClassesDocImpl();
        methodDoc = new GetMethodDocImpl();
        variableDoc = new GetVariableDocImpl();
        fileOutput = new FileOutputImpl();
    }*/

    public PackageDocsImpl(GetClassesDoc classesDoc, GetMethodDoc methodDoc, GetVariableDoc variableDoc, FileOutput fileOutput, PropertiesHandler propertiesHandler) {
        this.classesDoc = classesDoc;
        this.methodDoc = methodDoc;
        this.variableDoc = variableDoc;
        this.fileOutput = fileOutput;
        this.propertiesHandler = propertiesHandler;
    }

    //http://web.mit.edu/java_v1.5.0_22/distrib/share/docs/guide/javadoc/doclet/spec/index.html?com/sun/javadoc/package-summary.html
    //https://docs.oracle.com/javase/6/docs/technotes/tools/solaris/javadoc.html
    @Override
    public void packageDoc(RootDoc rootDoc) {
//        init();
        ClassDoc[] classes = classesDoc.getClasses(rootDoc);
        //遍历所有的类
        for (ClassDoc classDoc : classes) {
            //只扫描接口
            if (classDoc.isInterface()) {
                //接口名
                String interfaceName = classDoc.name();
                templateMap.put("interfaceName", interfaceName);
                //接口全类名
                templateMap.put("qualifiedName", classDoc.qualifiedName());
                //接口注释
                String interfaceComment = classDoc.commentText();
                templateMap.put("interfaceComment", interfaceComment);
                //方法列表
                List<Map<String, Object>> methodList = getMethodList(classDoc);
                templateMap.put("methodList", methodList);

                //遍历集合中的类
                ergodicSet(variableMap);

                Template template = TemplateConfig.config();
                root.put("templateMap", templateMap);
                root.put("variableMap", variableMap);
                root.put("variableTypeSet",variableTypeSet);
                fileOutput.output(template, root, interfaceName,propertiesHandler.getWikiPath());
                variableTypeSet.clear();
                mapperToClassDoc.clear();
            }
        }
    }

    /**
     * 判断一个类是否是泛型类,是泛型则返回泛型
     *
     * @param fieldName
     * @param classDoc
     * @return java.lang.reflect.Type
     */
    public java.lang.reflect.Type[] getGeneric(String fieldName, ClassDoc classDoc) {
        java.lang.reflect.Type[] actualTypeArguments = null;
        try {
            Class<?> aClass = Class.forName(classDoc.qualifiedName());
            Field declaredField = aClass.getDeclaredField(fieldName);
            java.lang.reflect.Type genericType = declaredField.getGenericType();
            actualTypeArguments = ((java.lang.reflect.ParameterizedType) genericType).getActualTypeArguments();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (ClassCastException c) {
            return null;
        }catch (NoSuchFieldException n){
            n.printStackTrace();
        }
        return actualTypeArguments;
    }

    /**
     * 获取类的方法列表
     * @param classDoc
     * @return list
     */
    public List<Map<String, Object>> getMethodList(ClassDoc classDoc){
        List<Map<String, Object>> methodList = new ArrayList<>();
        MethodDoc[] methodDocs = methodDoc.getMethods(classDoc);
        for (MethodDoc method : methodDocs) {
            Map<String, Object> methodMap = getMethodMap(method);

            //拿到方法返回值类中的变量列表
            String qualifiedRspName = (String) methodMap.get("qualifiedRspName");
            List<Map<String, Object>> rspVariableList=new ArrayList<>();
            if(!(BaseDateTypeHandler.matchBaseDateType(qualifiedRspName)||MatchUtil.match(qualifiedRspName, propertiesHandler.getBlackSet()))) {
                ClassDoc rspClass = classDoc.findClass(qualifiedRspName);
//                System.out.println("***************************"+qualifiedRspName+"*************************");
                rspVariableList = getVariableList(rspClass);
            }
            variableMap.put(qualifiedRspName, rspVariableList);
            //将出参类的所有父类的变量列表放进子类的变量列表中,排除基本类型和指定排除的类型
            if(!(BaseDateTypeHandler.matchBaseDateType(qualifiedRspName)||MatchUtil.match(qualifiedRspName, propertiesHandler.getBlackSet()))) {
                //当前类的出参父类中的变量封装到当前类
                packageSuperClassVariable(classDoc.findClass(qualifiedRspName));
                //父类中的出参泛型封装到当前类的wiki显示中
                java.lang.reflect.Type[] superGeneric = getSuperGeneric(classDoc.findClass(qualifiedRspName));
                packageGeneric(superGeneric,classDoc.findClass(qualifiedRspName).superclass());
            }

            //存放参数类型
            List<String> parameterType = new ArrayList<>();
            methodMap.put("parameterTypeList", parameterType);
            Parameter[] methodParameters = method.parameters();

            //拿到方法入参的所有的class中的变量列表
            for (Parameter methodParameter : methodParameters) {
                //根据参数全类名查找
                String name = methodParameter.type().qualifiedTypeName();
                //方法的参数不能为基本类型和指定的排除类型
                if(!(BaseDateTypeHandler.matchBaseDateType(name)||MatchUtil.match(name, propertiesHandler.getBlackSet()))) {
                    ClassDoc parameterClass = classDoc.findClass(name);

                    //参数名，非全类名
                    parameterType.add(parameterClass.qualifiedName());
                    List<Map<String, Object>> variableList = getVariableList(parameterClass);

                    //另一个数据模型map
                    variableMap.put(parameterClass.qualifiedName(), variableList);

                    //将入参的所有父类的变量列表放进入参类的列表当中
                    packageSuperClassVariable(parameterClass);
                }
            }
            methodList.add(methodMap);
        }
        return methodList;
    }

    /**
     * 根据classDoc获取类中的变量信息列表
     *
     * @param parameterClass
     * @return java.util.List
     */
    public List<Map<String, Object>> getVariableList(ClassDoc parameterClass) {
        FieldDoc[] fields = variableDoc.getVariables(parameterClass);
        List<Map<String, Object>> variableList = new ArrayList<>();

        for (FieldDoc field : fields) {
            Map<String, Object> innerMap = new HashMap<>(16);
            //不将序列号输出出去
            if (!"serialVersionUID".equals(field.name())) {
                innerMap.put("variableName", field.name());
                innerMap.put("comment", field.commentText());
                String typeName = field.type().qualifiedTypeName();
                //变量的类型全类名
                int index = typeName.lastIndexOf(".");
                //如果为-1说明是基本类型，找不到.
                if (index == -1) {
                    innerMap.put("type", typeName);
                } else {
                    //若得到的泛型类型的尺寸大于0说明是泛型
                    java.lang.reflect.Type[] generic = getGeneric(field.name(), parameterClass);
                    String listType = catListElement(generic);
                    if(listType.isEmpty()){
                        innerMap.put("type", typeName.substring(index + 1));
                    }else{
                        innerMap.put("type",typeName.substring(index + 1)+listType);
                    }
                    packageGeneric(generic,parameterClass);
                    packageSet(typeName,parameterClass);
                }
                variableList.add(innerMap);
            }
        }
        return variableList;
    }

    /**
     * 根据methodDoc封装方法map
     *
     * @param method
     * @return java.util.Map
     */
    public Map<String, Object> getMethodMap(MethodDoc method) {
        Map<String, Object> methodMap = new HashMap<>(16);
        String methodName = method.name();
        String methodComment = method.commentText();
        Type methodRsp = method.returnType();
        //放入methodMap
        methodMap.put("methodName", methodName);
        methodMap.put("methodComment", methodComment);
        methodMap.put("methodRsp", methodRsp);
        //TODO 方法返回值和方法参数带泛型处理
        //出参全类名
        methodMap.put("qualifiedRspName", methodRsp.qualifiedTypeName());
        return methodMap;
    }

    /**
     * 根据黑名单确认哪些类型可以放到set中,set中不存在则放入
     * @param variableTypeStr
     * @return boolean
     */
    public boolean distinguish(String variableTypeStr){
//        PropertiesHandler propertiesHandler = new PropertiesHandler();
        Set<String> blackSet = propertiesHandler.getBlackSet();
        boolean blackSetFlag = MatchUtil.match(variableTypeStr, blackSet);
        return blackSetFlag;
    }

    /**
     * 包装class中扫描到的封装类型
     *
     * @param variableTypeStr
     */
    public void packageSet(String variableTypeStr,ClassDoc parameterClass){
        boolean blackSetFlag = distinguish(variableTypeStr);
        boolean variableSetFlag = variableTypeSet.contains(variableTypeStr);
        //不属于黑名单则放入最终遍历集合
        if(!(blackSetFlag||variableSetFlag)){
            variableTypeSet.add(variableTypeStr);
            mapperToClassDoc.put(variableTypeStr,parameterClass);
        }
    }

    /**
     * 泛型类型中泛型的封装
     *
     * @param generic
     */
    public void packageGeneric(java.lang.reflect.Type[] generic,ClassDoc parameterClass){
        if(generic!=null) {
//            PropertiesHandler propertiesHandler = new PropertiesHandler();
            Set<String> blackSet = propertiesHandler.getBlackSet();
            for (java.lang.reflect.Type type : generic) {
                String typeName = type.getTypeName();
                if(!("T".equals(typeName)||"E".equals(typeName)||"?".equals(typeName)||MatchUtil.match(typeName,blackSet))){
                    packageSet(typeName,parameterClass);
                }
            }
        }
    }

    /**
     * 遍历set中的元素，并且在遍历的时候动态加入元素
     *
     * @param variableMap
     */
    public void ergodicSet(Map<String,Object> variableMap){
        //iterator方式不支持动态添加操作，若要使用动态添加只能使用一般写法
        for (int index=0;index<variableTypeSet.size();index++) {
            ClassDoc classDoc = (ClassDoc) mapperToClassDoc.get(variableTypeSet.get(index));
            ClassDoc aClass = classDoc.findClass(variableTypeSet.get(index));
            List<Map<String, Object>> variableList = getVariableList(aClass);
//            System.out.println(variableTypeSet.get(variableTypeSet.size()-1));
            variableMap.put(aClass.qualifiedName(),variableList);
        }
    }

    /**
     * 拼接变量的泛型参数
     *
     * @param generic
     * @return java.lang.String
     */
    public String catListElement(java.lang.reflect.Type[] generic){
        StringBuilder stringBuilder = new StringBuilder();
        if(generic!=null){
            stringBuilder.append("<");
            for (java.lang.reflect.Type type : generic) {
                int index = type.getTypeName().lastIndexOf(".");
                String typeName;
                if(index!=-1){
                    typeName=type.getTypeName().substring(index+1);
                }else{
                    typeName=type.getTypeName();
                }
                if("<".equals(stringBuilder.toString())) {
                    stringBuilder.append(typeName);
                }else{
                    stringBuilder.append(","+typeName);
                }
            }
            stringBuilder.append(">");
        }
        return stringBuilder.toString();
    }

    /**
     * 组装父类的变量列表到子类的列表中
     *
     * @param classDoc
     * @return 返回是否操作成功
     */
    public boolean packageSuperClassVariable(ClassDoc classDoc){
        if(classDoc.isClass()) {
            Object o = variableMap.get(classDoc.qualifiedName());
            //判断object是一个什么类型
            if (o instanceof List) {
                List<Map<String, Object>> variableList = (List<Map<String, Object>>) o;
                //拿到这个类的继承链
                LinkedList<ClassDoc> extendsChain = getExtendsChain(classDoc);
                for (ClassDoc doc : extendsChain) {
                    List<Map<String, Object>> superVariableList = getVariableList(doc);
                    //将父类中的变量加入到子类的list中
                    for (Map<String, Object> element : superVariableList) {
                        variableList.add(element);
                    }
                }
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * 获取某个类的继承链,如果classDoc拿不到泛型类型，可以考虑使用反射
     * @param classDoc
     * @return LinkedList<ClassDoc> 父类型的继承链
     */
//    public static LinkedList<ClassDoc> getExtendsChain(ClassDoc classDoc){
    public LinkedList<ClassDoc> getExtendsChain(ClassDoc classDoc){
        LinkedList<ClassDoc> extendsChain = new LinkedList<>();
//        PropertiesHandler propertiesHandler = new PropertiesHandler();
        Set<String> blackSet = propertiesHandler.getBlackSet();
        ClassDoc superclass = classDoc.superclass();

        //如果是jdk自带类型则忽略，只保留第三方类型，通过不断的修改superclass拿到父类继承连
        while (!MatchUtil.match(superclass.qualifiedName(), blackSet)){
            extendsChain.add(superclass);
            superclass=superclass.superclass();
        }
        return extendsChain;
    }

    /**
     * 使用反射获取父类的泛型
     * @param classDoc
     * @return 父类型的泛型类型
     */
    public java.lang.reflect.Type[] getSuperGeneric(ClassDoc classDoc){
        java.lang.reflect.Type[] actualTypeArguments = null;
        try {
            Class<?> aClass = Class.forName(classDoc.qualifiedName());
            java.lang.reflect.Type genericSuperclass = aClass.getGenericSuperclass();
            actualTypeArguments = ((java.lang.reflect.ParameterizedType) genericSuperclass).getActualTypeArguments();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (ClassCastException c) {
            return null;
        }
        return actualTypeArguments;
    }
}
