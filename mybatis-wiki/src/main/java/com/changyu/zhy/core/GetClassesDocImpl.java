package com.changyu.zhy.core;

import com.changyu.zhy.api.GetClassesDoc;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.RootDoc;

/**
 * 描述：获取java文件中的类或接口
 *
 * @author zhanghaoyu
 * @date 2018/12/13 15:11
 **/
public class GetClassesDocImpl implements GetClassesDoc {
    @Override
    public ClassDoc[] getClasses(RootDoc rootDoc) {
        ClassDoc[] classes = rootDoc.classes();
        return classes;
    }
}
