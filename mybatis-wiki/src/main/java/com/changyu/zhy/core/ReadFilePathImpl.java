package com.changyu.zhy.core;

import com.changyu.zhy.api.ReadFilePath;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * 描述：文件处理
 *
 * @author zhanghaoyu
 * @date 2018/12/20 16:49
 **/
public class ReadFilePathImpl implements ReadFilePath {
    /**
     * 实例化一个java文件过滤器
     */
    private static final JavaFileFilter JAVA_FILE_FILTER=new JavaFileFilter();
    /**
     * 读取java文件全路径
     * @param modulePath
     * @return
     */
    @Override
    public File[] readJavaFilePath(String modulePath) {
        File file = new File(modulePath);
        ArrayList<File> fileList = new ArrayList<>();
        readFile(file,fileList);
        File[] files = new File[fileList.size()];
        return fileList.toArray(files);
    }

    /**
     * 读取指定文件目录下的所有文件及目录
     * @param file
     * @param files 包含所有java文件的数组
     * @return
     */
    @Override
    public void readFile(File file, List<File> files) {
        if(file.isDirectory()){
            File[] javaOrDirectory = file.listFiles(JAVA_FILE_FILTER);
            for (File javaOrDic : javaOrDirectory) {
                readFile(javaOrDic,files);
            }
        }else{
            files.add(file);
        }
    }

    /**
     * 将file[]中的文件名全部连接起来
     * @param files
     * @return 全文件路径
     */
    @Override
    public String concatFilePath(File[] files) {
        StringBuilder stringBuilder = new StringBuilder();
        for (File file : files) {
            stringBuilder.append("\""+file.getAbsolutePath()+"\" ");
        }
        return stringBuilder.toString();
    }

    /**
     * 拼接javadoc命令
     * @param elementStr
     * @return 命令字符串
     */
    @Override
    public String concatCommandLine(String...elementStr) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String element : elementStr) {
            stringBuilder.append(element+" ");
        }
        return stringBuilder.toString();
    }
}
