package com.changyu.zhy.core;

import com.changyu.zhy.api.FileOutput;
import com.changyu.zhy.api.PackageDocs;
import com.changyu.zhy.api.ReadFilePath;
import com.changyu.zhy.config.PropertiesHandler;
import com.sun.javadoc.Doclet;
import com.sun.javadoc.RootDoc;

import java.io.File;
import java.util.Properties;

/**
 * 描述：生成wiki标记文档
 *
 * @author zhanghaoyu
 * @date 2018/12/13 15:36
 **/
public class GenerateWikiDoc {
    private static RootDoc rootDoc;
    private static PackageDocs packageDocs;

    /**
     * 从javadoc命令中获取源路径下的所有根信息
     */
    public static class Doclet {
        public static boolean start(RootDoc rootDoc) {
            GenerateWikiDoc.rootDoc = rootDoc;
            return true;
        }
    }

    public static void main(String[] args) {
        //配置文件读取
        PropertiesHandler propertiesHandler = new PropertiesHandler();
        Properties properties = propertiesHandler.readProperties();
        propertiesHandler.packageProperties(properties);
        FileOutput fileOutput = new FileOutputImpl();
        //生成命令临时文件
        String tempFilePath = fileOutput.outputTemporary(propertiesHandler.getMoubldeName());
        com.sun.tools.javadoc.Main.execute(GenerateWikiDoc.class.getClassLoader(), new String[]{"-private", "-doclet",
                Doclet.class.getName(),
                "@" + tempFilePath, "-verbose"});
        //删除命令临时文件
        fileOutput.deleteTemporary(tempFilePath);


        packageDocs = new PackageDocsImpl(new GetClassesDocImpl(),
                new GetMethodDocImpl(),
                new GetVariableDocImpl(),
                new FileOutputImpl(),
                propertiesHandler);
        packageDocs.packageDoc(rootDoc);
    }
}
