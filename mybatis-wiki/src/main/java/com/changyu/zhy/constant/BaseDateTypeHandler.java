package com.changyu.zhy.constant;

/**
 * 描述：基本数据类型处理
 *
 * @author zhanghaoyu
 * @date 2018/12/22 20:12
 **/
public class BaseDateTypeHandler {
    public static final String VOID="void";

    public static final String[] baseType=new String[]{"int","long","double","float","char","short","byte","boolean","void"};
    public static boolean matchBaseDateType(String strType){
        for (String type : baseType) {
            if(type.equals(strType)){
                return true;
            }
        }
        return false;
    }
}
