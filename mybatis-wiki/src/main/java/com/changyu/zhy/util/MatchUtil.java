package com.changyu.zhy.util;

import java.util.Set;

/**
 * 描述：字符串正则匹配
 *
 * @author zhanghaoyu
 * @date 2018/12/19 11:38
 **/
public class MatchUtil {
    /**
     * 根据指定的内容和不定的pattern数量进行匹配
     *
     * @param content
     * @param pattern
     * @return
     */
    public static boolean match(String content,Set<String> pattern){
        for (String pat : pattern) {
            boolean matchFlag = content.matches(pat);
            if(matchFlag){
                return true;
            }
        }
        return false;
    }
}
