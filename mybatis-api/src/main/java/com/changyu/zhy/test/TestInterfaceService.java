package com.changyu.zhy.test;

import com.changyu.zhy.bo.TestInterfaceReqBO;
import com.changyu.zhy.bo.TestInterfaceRspBO;

import java.util.Map;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/4 15:11
 **/
public interface TestInterfaceService {
    TestInterfaceRspBO test(TestInterfaceReqBO testInterfaceReqBO);
   // TestInterfaceRspBO[] testArray(TestInterfaceReqBO[] testInterfaceReqBOS);
}
