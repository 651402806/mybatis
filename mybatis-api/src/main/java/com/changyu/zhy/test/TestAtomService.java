package com.changyu.zhy.test;

import com.changyu.zhy.bo.TestInterfaceReqBO;
import com.changyu.zhy.bo.TestInterfaceRspBO;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/4 19:54
 **/
public interface TestAtomService {
    TestInterfaceRspBO testAtom(TestInterfaceReqBO testInterfaceReqBO);
}
