package com.changyu.zhy.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/5/7 21:43
 **/
@Data
public class SentimentCountReqBO implements Serializable {
    private Long weiboId;
}
