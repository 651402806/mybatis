package com.changyu.zhy.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/5 17:05
 **/
@Data
public class BlogtypeBO implements Serializable {
    private static final long serialVersionUID = 2814054410042817853L;
    private String id;
    private String typeName;
}
