package com.changyu.zhy.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/4 15:15
 **/
@Data
public class TestInterfaceRspBO extends RspBaseBO {
    private static final long serialVersionUID = 5850991908798368826L;
    /**
     * id
     */
    private String id;
    /**
     * 测试字段
     */
    public static final String test=null;
    /**
     * 列表
     */
    private List<BlogtypeBO> blogtypeBOList;
}
