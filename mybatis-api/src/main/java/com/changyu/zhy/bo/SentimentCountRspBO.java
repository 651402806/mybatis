package com.changyu.zhy.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/5/7 21:40
 **/
@Data
public class SentimentCountRspBO implements Serializable {
    private Integer positiveCount;
    private Integer negativeCount;
    private List<CommentTableBO> positiveCommentList;
    private List<CommentTableBO> negativeCommentList;
}
