package com.changyu.zhy.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/4 15:14
 **/
@Data
public class TestInterfaceReqBO implements Serializable {
    private static final long serialVersionUID = -7904398435510680935L;
    /**
     * 类型名
     */
    private String typeName;
    /**
     * 测试list
     */
    private List<String> listString;
    /**
     * 测试数组
     */
    private String[] testString;
}
