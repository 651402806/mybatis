package com.changyu.zhy.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 21:20
 **/
@Data
public class QueryReqBO implements Serializable {
    String queryContent;
    Integer pageNo;
    Integer pageSize;
    /**
     * 是否返回全部的list
     */
    Boolean returnAllFlag=false;
    /**
     * 是否转码标志
     */
    Boolean convert=false;
}
