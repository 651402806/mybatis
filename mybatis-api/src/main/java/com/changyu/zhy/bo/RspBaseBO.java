package com.changyu.zhy.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 描述：出参基类
 *
 * @author zhanghaoyu
 * @date 2018/12/8 22:53
 **/
@Data
public class RspBaseBO implements Serializable {
    private static final long serialVersionUID = -3736903725124663336L;
    /**
     * 返回编码
     */
    private String responseCode;
    /**
     * 返回描述
     */
    private String responseDesc;
}
