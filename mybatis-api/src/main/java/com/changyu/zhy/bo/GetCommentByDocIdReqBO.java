package com.changyu.zhy.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/30 20:34
 **/
@Data
public class GetCommentByDocIdReqBO implements Serializable {
    /**
     * 微博id
     */
    public String docId;
    /**
     * 页码
     */
    public Integer pageNo;
    /**
     * 页面大小
     */
    public Integer pageSize;
    /**
     * 评论情感极性
     */
    private Integer emotionalPolarity;
}
