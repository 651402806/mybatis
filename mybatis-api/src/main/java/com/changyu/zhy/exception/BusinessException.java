package com.changyu.zhy.exception;

import lombok.Data;

/**
 * 描述：异常类
 *
 * @author zhanghaoyu
 * @date 2018/12/8 23:00
 **/
@Data
public class BusinessException extends RuntimeException{
    private static final long serialVersionUID = 6634944748752435391L;
    /**
     * 异常编码
     */
    private String responseCode;
    /**
     * 异常描述
     */
    private String responseDesc;

    public BusinessException(String responseCode, String responseDesc) {
        this.responseCode = responseCode;
        this.responseDesc = responseDesc;
    }
}
