package com.changyu.zhy.nlp;

import java.util.List;
import java.util.Map;

/**
 * 描述：获取某微博文档中指定个数的关键词
 *
 * @author zhanghaoyu
 * @date 2019/5/19 16:20
 **/
public interface GetKeyWordService {
    /**
     *
     * @param topN 前n个
     * @param content 微博内容
     * @return 关键词列表
     */
    List<Map<String, Object>> getTopNKeyWord(Integer topN, String content);
}
