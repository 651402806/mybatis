package com.changyu.zhy.nlp;

/**
 * 描述：创建倒排索引
 *
 * @author zhanghaoyu
 * @date 2019/4/23 14:16
 **/
public interface CreateInvertedIndexService {
    void createInvertedIndex();
}
