package com.changyu.zhy.nlp;

import com.changyu.zhy.bo.CommentTableBO;
import com.changyu.zhy.bo.GetCommentByDocIdReqBO;

import java.util.List;

/**
 * 描述：根据文章id返回评论列表(分页查询)
 *
 * @author zhanghaoyu
 * @date 2019/4/30 20:27
 **/
public interface GetCommentByDocIdService {
    /**
     * 根据微博id查询评论列表
     * @return 评论列表
     */
    List<CommentTableBO> findCommentByDocId(GetCommentByDocIdReqBO getCommentByDocIdReqBO);
}
