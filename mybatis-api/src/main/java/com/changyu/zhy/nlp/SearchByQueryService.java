package com.changyu.zhy.nlp;

import com.changyu.zhy.bo.DataBO;
import com.changyu.zhy.bo.QueryReqBO;

import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 17:18
 **/
public interface SearchByQueryService {
    /**
     * 根据查询内容查询微博文本，并返回
     * @param queryReqBO
     * @return
     */
    List<DataBO> query(QueryReqBO queryReqBO);
}
