package com.changyu.zhy.nlp;

import com.changyu.zhy.bo.SentimentCountReqBO;
import com.changyu.zhy.bo.SentimentCountRspBO;

/**
 * 描述：查询消极，积极评论条数
 *
 * @author zhanghaoyu
 * @date 2019/5/7 21:37
 **/
public interface SentimentSearchService {
    SentimentCountRspBO getSentimentCount(SentimentCountReqBO sentimentCountReqBO);
}
