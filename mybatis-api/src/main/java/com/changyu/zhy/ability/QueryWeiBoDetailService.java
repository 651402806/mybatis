package com.changyu.zhy.ability;

import com.changyu.zhy.bo.DataBO;

/**
 * 描述：根据微博id查询微博详情
 *
 * @author zhanghaoyu
 * @date 2019/5/1 21:09
 **/
public interface QueryWeiBoDetailService {
    DataBO queryWeiboByDocId(DataBO dataBO);
}
