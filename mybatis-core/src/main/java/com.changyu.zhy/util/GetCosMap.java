package com.changyu.zhy.util;

import com.changyu.zhy.bo.DataBO;

import java.util.*;

/**
 * 描述：根据所有文档的内容返回一个以（文本内容：相似度）的map
 *
 * @author zhanghaoyu
 * @date 2019/5/9 20:16
 **/
public class GetCosMap {
    /**
     * 主函数
     * @param documentList
     * @param queryContent 查询内容的文本
     * @return
     */
    public static Map<String,Double> caculate(List<DataBO> dataBOList, String queryContent){
        List<String> documentList = new ArrayList<>();
        for (DataBO dataBO : dataBOList) {
            documentList.add(dataBO.getWeiboContent());
        }
        HashMap<String, Double> similarMap = new HashMap<>();
        //将queryContent放到最后一起去计算tf-idf
        documentList.add(queryContent);
        TfIdf tfIdf = new TfIdf(documentList);
        List<Set<String>> documentExistWord=new ArrayList<>();

//      存放切分好的wordList的总list
        ArrayList<List<String>> allWordList = new ArrayList<>();
        for (String s : documentList) {
            List<String> wordList = tfIdf.splitWord(s);
            allWordList.add(wordList);
        }

        //所有文档的单词跟tf-idf的列表
        List<Map<String, Double>> tfIdfMapList = new ArrayList<>();
        for (List<String> wordList : allWordList) {
            documentExistWord.add(new HashSet<>(wordList));
            //计算tf
            Map<String, Double> tfMap = tfIdf.calculateTf(wordList);
//            System.out.println("tfMap:数据"+tfMap.toString());
            Map<String, Double> tfIdfMap = tfIdf.calculateIdf(documentList.size(), tfMap, allWordList);
//            System.out.println("tfIdfMap数据："+tfIdfMap.toString());
            tfIdfMapList.add(tfIdfMap);
        }

        //取出tfIdfMapList的最后一个查询文本的tf-idf，并计算他跟其他文档的相似度
        Map<String, Double> queryContentTfIdf = tfIdfMapList.get(tfIdfMapList.size() - 1);
        for (int index = 0; index < tfIdfMapList.size() - 1; index++) {
            double similar = Vsm.calculateCos(tfIdfMapList.get(index), queryContentTfIdf);
            similarMap.put(dataBOList.get(index).getId().toString(),similar);
        }
        return similarMap;
    }
}
