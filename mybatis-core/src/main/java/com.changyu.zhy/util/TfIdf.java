package com.changyu.zhy.util;

import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.SegToken;

import java.util.*;

/**
 * 描述：计算文本tf-idf值
 *
 * @author zhanghaoyu
 * @date 2019/5/8 22:17
 **/
public class TfIdf {
    /**
     * 文档列表集合，string为一个文档的内容
     */
    private List<String> documentList;
    /**
     * （关键词，词频）字典集合
     */
    private Map<String,Double> tfMap;
    /**
     * 每一个set都存储这个文档中出现过的单词，由于统计包含某个词的文档数
     */
    private List<Set<String>> documentExistWord;
    private Set<String> stopWordSet;
    public TfIdf(List<String> documentList) {
        this.documentList=documentList;
        this.stopWordSet = new StopWord().getStopWordSet();
    }

    /**
     * 切分文档内容获取单词列表
     * @param documentContent
     * @return
     */
     public List<String> splitWord(String documentContent){
         JiebaSegmenter segmenter = new JiebaSegmenter();
         //切分的word列表
         List<SegToken> tokenList = segmenter.process(documentContent, JiebaSegmenter.SegMode.SEARCH);
         List<String> wordList = new ArrayList<>();
         for (SegToken segToken : tokenList) {
             //去除停用词
             if(!stopWordSet.contains(segToken.word)) {
                 wordList.add(segToken.word);
             }
         }
         return wordList;
     }

    /**
     *
     * @param wordList 单个文档单词列表
     * @return tf列表
     */
    public Map<String,Double> calculateTf(List<String> wordList){
//        List<String> wordList = splitWord(documentContent);
        //文章总词数
        Integer wordCount=wordList.size();
        Map<String,Integer> countMap = new HashMap<String,Integer>();
        for(String word : wordList) {
            if(countMap.containsKey(word)) {
                countMap.put(word, countMap.get(word)+1);
            }else {
                countMap.put(word, 1);
            }
        }

//        System.out.println("countMap数据"+countMap.toString());
//        System.out.println("wordCount值"+wordCount);
        //计算词频
        tfMap=new HashMap<>();
        for (Map.Entry<String, Integer> stringIntegerEntry : countMap.entrySet()) {
            double wordTf=(double)stringIntegerEntry.getValue()/wordCount;
            tfMap.put(stringIntegerEntry.getKey(),wordTf);
        }
        return tfMap;
    }

    /**
     *
     * @param documentCount 语料库文档总数
     */
    public Map<String, Double> calculateIdf(Integer documentCount,Map<String,Double> tfMap,List<List<String>> documentExistWord){
        Map<String, Double> tfIdfMap = new HashMap<>();
        for (Map.Entry<String, Double> wordMap : tfMap.entrySet()) {
            Integer currentAppear=0;
            //遍历文档分词结果，确定有几个文档包含该词
            for (List<String> documentWord : documentExistWord) {
                if(documentWord.contains(wordMap.getKey())){
                    currentAppear+=1;
                }
            }
//            System.out.println(wordMap.getKey()+":"+currentAppear);
            //除1+出现文档次数存在负数，有这个防止除0，不加一时全是正数
            double idf = Math.log((double) documentCount / (currentAppear+1));
            double tfIdf=wordMap.getValue()*idf;
//            System.out.println(wordMap.getKey()+":"+idf);
            tfIdfMap.put(wordMap.getKey(),tfIdf);
        }
        return tfIdfMap;
    }



//    public static void main(String[] args) {
//        ArrayList<String> documentList = new ArrayList<>();
//        documentList.add("孩子上了幼儿园 安全防拐教育要做好");
////        documentList.add("我经常玩手机");
////        documentList.add("我手机");
////        documentList.add("乔布斯推动了世界，ipod、iphone、ipad、ipad2,一款一款接连不断");
////        documentList.add("乔布斯出门买了四袋苹果");
////        documentList.add("乔布斯ipad2");
//        TfIdf tfIdf = new TfIdf(documentList);
//        List<Set<String>> documentExistWord=new ArrayList<>();
//
////      存放切分好的wordList的总list
//        ArrayList<List<String>> allWordList = new ArrayList<>();
//        for (String s : documentList) {
//            List<String> wordList = tfIdf.splitWord(s);
//            allWordList.add(wordList);
//        }
//
//        List<Map<String, Double>> tfIdfMapList = new ArrayList<>();
//        for (List<String> wordList : allWordList) {
//            documentExistWord.add(new HashSet<>(wordList));
//            //计算tf
//            Map<String, Double> tfMap = tfIdf.calculateTf(wordList);
////            System.out.println("tfMap:数据"+tfMap.toString());
//            Map<String, Double> tfIdfMap = tfIdf.calculateIdf(documentList.size(), tfMap, allWordList);
//            System.out.println("tfIdfMap数据："+tfIdfMap.toString());
//            tfIdfMapList.add(tfIdfMap);
//        }
//
////        Map<String, Double> d1 = tfIdfMapList.get(0);
//////        Map<String, Double> d2 = tfIdfMapList.get(2);
////
////        Map<String, Double> d3 = new HashMap<>();
////        Map<String, Double> d4 = new HashMap<>();
////        double v = Vsm.calculateCos(d1, d2);
////        System.out.println(v);
//    }
}
