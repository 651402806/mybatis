package com.changyu.zhy.util;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/5/9 18:54
 **/
public class Vsm {
//    public static double calCosSim(Map<String, Double> v1, Map<String, Double>
//            v2) {
//
//        double sclar = 0.0,norm1=0.0,norm2=0.0,similarity=0.0;
//
//        Set<String> v1Keys = v1.keySet();
//        Set<String> v2Keys = v2.keySet();
//
//        Set<String> both= new HashSet<>();
//        both.addAll(v1Keys);
//        both.retainAll(v2Keys);
//        System.out.println(both);
//
//        for (String str1 : both) {
//            sclar += v1.get(str1) * v2.get(str1);
//        }
//
//        for (String str1:v1.keySet()){
//            norm1+=Math.pow(v1.get(str1),2);
//        }
//        for (String str2:v2.keySet()){
//            norm2+=Math.pow(v2.get(str2),2);
//        }
//
//        similarity=sclar/Math.sqrt(norm1*norm2);
//        System.out.println("sclar:"+sclar);
//        System.out.println("norm1:"+norm1);
//        System.out.println("norm2:"+norm2);
//        System.out.println("similarity:"+similarity);
//        return similarity;
//    }

    /**
     *
     * @param doc1
     * @param doc2
     * @return
     */
    public static double calculateCos(Map<String,Double> doc1,Map<String,Double> doc2){
        Set<String> doc1Key = doc1.keySet();
        Set<String> doc2Key = doc2.keySet();
        HashSet<String> allKey = new HashSet<>();
        allKey.addAll(doc1Key);
        allKey.addAll(doc2Key);

        double itemAdd=0;
        //遍历key集合
        for (String key : allKey) {
            //获取同key的值并统计所有文档相同key的值相乘后的乘积
            Double item1 = doc1.get(key);
            Double item2 = doc2.get(key);
            itemAdd+=assign(item1)*assign(item2);
        }

        //统计doc1中的所有词的平方和
        double doc1Sum = caculatePowSum(doc1);
        double doc2Sum = caculatePowSum(doc2);
        double divide = Math.sqrt(doc1Sum * doc2Sum);
        if(divide==0){
            return 0;
        }else{
            return itemAdd/divide;
        }
    }

    public static Double assign(Double item){
        if(item==null){
            return 0.0;
        }else{
            return item;
        }
    }

    /**
     * 统计某文档中所有词项的平方和
     * @param doc
     * @return
     */
    public static double caculatePowSum(Map<String,Double> doc){
        double sum=0;
        for (Double value : doc.values()) {
            sum+=Math.pow(value,2);
        }
        return sum;
    }
}
