package com.changyu.zhy.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/**
 * 描述：返回停用词列表
 *
 * @author zhanghaoyu
 * @date 2019/5/19 15:56
 **/
public class StopWord {
    static HashSet<String> stopWordsSet;
    public Set<String> getStopWordSet(){
        if(stopWordsSet==null){
            stopWordsSet=new HashSet<>();
            loadStopWords(stopWordsSet, this.getClass().getResourceAsStream("/stop_words.txt"));
        }
        stopWordsSet.add(" ");
        return stopWordsSet;
    }
    private void loadStopWords(Set<String> set, InputStream in){
        BufferedReader bufr;
        try
        {
            bufr = new BufferedReader(new InputStreamReader(in,"utf-8"));
            String line=null;
            while((line=bufr.readLine())!=null) {
                set.add(line.trim());
            }
            try
            {
                bufr.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        StopWord stopWord = new StopWord();
        System.out.println(stopWord.getStopWordSet().toString());
    }
}
