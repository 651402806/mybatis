package com.changyu.zhy.nlp;

import com.changyu.zhy.bo.CommentTableBO;
import com.changyu.zhy.bo.SentimentCountReqBO;
import com.changyu.zhy.bo.SentimentCountRspBO;
import com.changyu.zhy.dao.CommentTableMapper;
import com.changyu.zhy.po.CommentTablePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/5/7 21:42
 **/
@Service("sentimentSearchService")
public class SentimentSearchServiceImpl implements SentimentSearchService{
    @Autowired
    private CommentTableMapper commentTableMapper;
    @Override
    public SentimentCountRspBO getSentimentCount(SentimentCountReqBO sentimentCountReqBO) {
        SentimentCountRspBO sentimentCountRspBO = new SentimentCountRspBO();
        CommentTablePO commentTablePO = new CommentTablePO();
        commentTablePO.setWeiboId(sentimentCountReqBO.getWeiboId());
        //首先查询情感极性为正向0的
        commentTablePO.setEmotionalPolarity(0);
        Integer positiveCount=commentTableMapper.getCount(commentTablePO);
        sentimentCountRspBO.setPositiveCount(positiveCount);
        //查询积极情感列表
        List<CommentTablePO> positiveCommentList = commentTableMapper.getList(commentTablePO);

        sentimentCountRspBO.setPositiveCommentList(convert(positiveCommentList));

        //消极评论
        commentTablePO.setEmotionalPolarity(1);
        Integer negativeCount=commentTableMapper.getCount(commentTablePO);
        sentimentCountRspBO.setNegativeCount(negativeCount);
        //查询消极评论列表
        List<CommentTablePO> negativeCommentList = commentTableMapper.getList(commentTablePO);
        sentimentCountRspBO.setNegativeCommentList(convert(negativeCommentList));

        return sentimentCountRspBO;
    }

    public List<CommentTableBO> convert(List<CommentTablePO> commentTablePOList){
        ArrayList<CommentTableBO> commentTableBOArrayList = new ArrayList<>();
        for (CommentTablePO commentTablePO : commentTablePOList) {
            CommentTableBO commentTableBO = new CommentTableBO();
            BeanUtils.copyProperties(commentTablePO,commentTableBO);
            commentTableBOArrayList.add(commentTableBO);
        }
        return commentTableBOArrayList;
    }
}
