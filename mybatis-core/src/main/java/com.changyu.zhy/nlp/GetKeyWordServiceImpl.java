package com.changyu.zhy.nlp;

import com.changyu.zhy.bo.DataBO;
import com.changyu.zhy.dao.RawDataMapper;
import com.changyu.zhy.po.DataPO;
import com.changyu.zhy.util.TfIdf;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 描述：前topN关键词
 *
 * @author zhanghaoyu
 * @date 2019/5/19 16:22
 **/
@Service("getKeyWordService")
public class GetKeyWordServiceImpl implements GetKeyWordService{
    @Autowired
    private RawDataMapper rawDataMapper;
    @Override
    public List<Map<String, Object>> getTopNKeyWord(Integer topN, String content) {
        List<DataPO> all = rawDataMapper.getAll();
        List<String> documentList = new ArrayList<>();
        for (DataPO dataPO : all) {
            documentList.add(dataPO.getWeiboContent());
        }
        TfIdf tfIdf = new TfIdf(documentList);
        ArrayList<List<String>> allWordList = new ArrayList<>();
        for (String s : documentList) {
            List<String> wordList = tfIdf.splitWord(s);
            allWordList.add(wordList);
        }

        //对当前微博文本进行分词
        List<String> contentWords = tfIdf.splitWord(content);
        //计算当前微博文本的tf
        Map<String, Double> tfMap = tfIdf.calculateTf(contentWords);
        Map<String, Double> tfIdfMap = tfIdf.calculateIdf(documentList.size() - 1, tfMap, allWordList);
        List<Map.Entry<String, Double>> entries = sortMap(tfIdfMap);
        if(topN>entries.size()){
           entries= entries.subList(0,entries.size()-1);
        }else{
            entries=entries.subList(0,topN);
        }
        List<Map<String,Object>> topNList = new ArrayList<>();
//        for (Map.Entry<String, Double> entry : entries) {
//            topNList.add(entry.getKey());
//        }
//        return topNList;
        for (Map.Entry<String, Double> entry : entries) {
            Map<String, Object> tempMap = new HashMap<>();
            tempMap.put("text",entry.getKey());
            tempMap.put("weight",entry.getValue());
            topNList.add(tempMap);
        }
        return topNList;
    }

    public static ArrayList<Map.Entry<String, Double>> sortMap(Map<String,Double> map){
        ArrayList<Map.Entry<String, Double>> list = new ArrayList<>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            //降序
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        return list;
    }
}
