package com.changyu.zhy.nlp;

import com.changyu.zhy.bo.CommentTableBO;
import com.changyu.zhy.bo.GetCommentByDocIdReqBO;
import com.changyu.zhy.dao.CommentTableMapper;
import com.changyu.zhy.po.CommentTablePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 描述：根据微博的id查找评论列表
 *
 * @author zhanghaoyu
 * @date 2019/4/30 20:32
 **/
@Service("getCommentByDocIdService")
public class GetCommentByDocIdServiceImpl implements GetCommentByDocIdService{
    @Autowired
    private CommentTableMapper commentTableMapper;
    @Override
    public List<CommentTableBO> findCommentByDocId(GetCommentByDocIdReqBO getCommentByDocIdReqBO) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("pageNo",getCommentByDocIdReqBO.getPageNo()*getCommentByDocIdReqBO.getPageSize());
        map.put("pageSize",getCommentByDocIdReqBO.getPageSize());
        map.put("weiboId",getCommentByDocIdReqBO.getDocId());
        map.put("emotionalPolarity",getCommentByDocIdReqBO.getEmotionalPolarity());
        List<CommentTablePO> listPage = commentTableMapper.getListPage(map);
        ArrayList<CommentTableBO> tableBOArrayList = new ArrayList<>();
        for (CommentTablePO commentTablePO : listPage) {
            CommentTableBO commentTableBO = new CommentTableBO();
            BeanUtils.copyProperties(commentTablePO,commentTableBO);
            tableBOArrayList.add(commentTableBO);
        }
        return tableBOArrayList;
    }
}
