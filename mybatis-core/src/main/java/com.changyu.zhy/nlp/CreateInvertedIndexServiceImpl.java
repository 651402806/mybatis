package com.changyu.zhy.nlp;

import com.changyu.zhy.dao.InvertIndexMapper;
import com.changyu.zhy.dao.CleanedDataMapper;
import com.changyu.zhy.po.InvertIndexPO;
import com.changyu.zhy.po.DataPO;
import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.SegToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 14:19
 **/
@Service("createInvertedIndexService")
public class CreateInvertedIndexServiceImpl implements CreateInvertedIndexService{
    @Autowired
    private CleanedDataMapper cleanedDataMapper;
    @Autowired
    private InvertIndexMapper invertIndexMapper;
    @Override
    public void createInvertedIndex() {
        Map<String, Object> invertMap = calculateInvertIndex();
        List<InvertIndexPO> invertIndexPOList = packageInvertIndexPO(invertMap);
        invertIndexMapper.insertIndex(invertIndexPOList);
    }


    public Map<String,Object> calculateInvertIndex(){
        List<DataPO> rawDataMapperList = cleanedDataMapper.getList(null);
        //存放已经存储过的单词的set
        HashSet<String> set = new HashSet<>();
        //倒排索引表
        HashMap<String, Object> invertMap = new HashMap<>(128);
        for (DataPO dataPO : rawDataMapperList) {
            String weiboContent = dataPO.getWeiboContent();
            String docId = dataPO.getId().toString();
            JiebaSegmenter segmenter = new JiebaSegmenter();
            List<SegToken> tokenList = segmenter.process(weiboContent, JiebaSegmenter.SegMode.SEARCH);
            Set<SegToken> tokenSet = removeDuplicate(tokenList);
            for (SegToken segToken : tokenSet) {
                //不存在
                if(!findWord(segToken.word,set)){
                    //存储doc_id和tf
                    HashMap<String, Object> docTf = new HashMap<>();
                    docTf.put("doc_id",docId);
                    docTf.put("tf",getCount(segToken.word,weiboContent));
                    //一个arraylist存储所有出现这个单词的doc信息
                    ArrayList<HashMap<String, Object>> docTfList = new ArrayList<>();
                    docTfList.add(docTf);
                    invertMap.put(segToken.word,docTfList);
                    set.add(segToken.word);
                }
                else{
                    //单词存在时
                    ArrayList<HashMap<String, Object>> docTfList = (ArrayList<HashMap<String, Object>>) invertMap.get(segToken.word);
                    //存储doc_id和tf
                    HashMap<String, Object> docTf = new HashMap<>();
                    docTf.put("doc_id",docId);
                    docTf.put("tf",getCount(segToken.word,weiboContent));
                    docTfList.add(docTf);
                }
            }
        }
        return invertMap;
    }

    /**
     * 检测一个单词在不在这个list中
     * @param word
     * @param set
     * @return boolean
     */
    public boolean findWord(String word, HashSet set){
        if(set.contains(word)){
            return true;
        }
        return false;
    }

    /**
     * 获取某个单词在文档中的频率
     * @param word
     * @param content
     * @return
     */
    public Integer getCount(String word,String content){
        int count = 0;
        word=word.replaceAll("[^a-zA-Z0-9\\u4E00-\\u9FA5]", "");
        Pattern p = Pattern.compile(word, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(content);
        while (m.find()) {
            count++;
        }
        return count;
    }

    /**
     * 列表去重
     * @param segTokenList
     * @return
     */
    public Set<SegToken> removeDuplicate(List<SegToken> segTokenList){
        Set<SegToken> set = new TreeSet<SegToken>(new Comparator<SegToken>() {
            @Override
            public int compare(SegToken o1, SegToken o2) {
                //字符串,则按照asicc码升序排列
                return o1.word.compareTo(o2.word);
            }
        });
        set.addAll(segTokenList);
        return set;
    }

    /**
     *
     * 组装po列表
     * @return
     */
    public List<InvertIndexPO> packageInvertIndexPO(Map<String, Object> invertMap){
        List<InvertIndexPO> indexPOArrayList = new ArrayList<>();
        for (Map.Entry<String, Object> entry : invertMap.entrySet()) {
            List<Object> docTfList = (List<Object>) entry.getValue();
            for (Object o : docTfList) {
                HashMap<String, Object> tfMap = (HashMap<String, Object>) o;
                InvertIndexPO invertIndexPO = new InvertIndexPO();
                invertIndexPO.setWord(entry.getKey());
                invertIndexPO.setDocId(Long.valueOf((String)tfMap.get("doc_id")));
                invertIndexPO.setTf(Integer.valueOf(String.valueOf(tfMap.get("tf"))));
                indexPOArrayList.add(invertIndexPO);
            }
        }
        return indexPOArrayList;
    }
}
