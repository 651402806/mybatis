package com.changyu.zhy.nlp;

import com.changyu.zhy.bo.DataBO;
import com.changyu.zhy.bo.QueryReqBO;
import com.changyu.zhy.dao.InvertIndexMapper;
import com.changyu.zhy.dao.RawDataMapper;
import com.changyu.zhy.po.DataPO;
import com.changyu.zhy.po.InvertIndexPO;
import com.changyu.zhy.util.GetCosMap;
import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.SegToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 17:43
 **/
@Service("searchByQueryService")
public class SearchByQueryServiceImpl implements SearchByQueryService{
    @Autowired
    private InvertIndexMapper invertIndexMapper;
    @Autowired
    private RawDataMapper rawDataMapper;

    @Override
    public List<DataBO> query(QueryReqBO queryReqBO) {
        try {
            if(queryReqBO.getConvert()) {
                queryReqBO.setQueryContent(new String(queryReqBO.getQueryContent().getBytes("ISO-8859-1"), "utf-8"));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        JiebaSegmenter segmenter = new JiebaSegmenter();
        List<SegToken> tokenList = segmenter.process(queryReqBO.getQueryContent(), JiebaSegmenter.SegMode.SEARCH);
        //不重复的词语集合
        Set<SegToken> segTokens = removeDuplicate(tokenList);
        //满足查询条件的docid合集
        HashSet<Long> docIdSet = new HashSet<>();
        for (SegToken segToken : segTokens) {
            InvertIndexPO invertIndexPO = new InvertIndexPO();
            invertIndexPO.setWord(segToken.word);

            List<InvertIndexPO> list = invertIndexMapper.getList(invertIndexPO);
            //如果docIdSet为空放入，否则取交集,取并集并计算查询文本和各个文本的相似度进行计算
//            if(docIdSet.isEmpty()){
            docIdSet.addAll(getDocIdList(list));
//            }
//            else{
//                docIdSet.retainAll(getDocIdList(list));
//            }
        }
//        System.out.println("去重后的集合"+docIdSet.toString());
        return orderByCos(docIdSet,queryReqBO.getPageNo(),queryReqBO.getPageSize(),queryReqBO.getQueryContent(),queryReqBO.getReturnAllFlag());
//        return selectByDocId(docIdSet,queryReqBO.getPageNo(),queryReqBO.getPageSize());
    }

    public List<DataBO> selectByDocId(HashSet<Long> docIdSet){
        ArrayList<Long> docIdList=null;
        if(docIdSet.size()!=0){
           docIdList = new ArrayList<>(docIdSet);
        }
        HashMap<String, Object> queryMap = new HashMap<>();
        queryMap.put("docIdList",docIdList);
//        queryMap.put("pageNo",pageNo*pageSize);
//        queryMap.put("pageSize",pageSize);
        List<DataPO> listPage = rawDataMapper.getListPage(queryMap);
        ArrayList<DataBO> dataBOArrayList = new ArrayList<>();
        for (DataPO dataPO : listPage) {
            DataBO dataBO = new DataBO();
            BeanUtils.copyProperties(dataPO,dataBO);
            dataBOArrayList.add(dataBO);
        }
//        System.out.println(dataBOArrayList.toString());
        return dataBOArrayList;
    }

    /**
     * 获取list中的docId合集
     * @param list
     * @return
     */
    public List<Long> getDocIdList(List<InvertIndexPO> list){
        List<Long> docIdList = new ArrayList<>();
        for (InvertIndexPO invertIndexPO : list) {
            docIdList.add(invertIndexPO.getDocId());
        }
        return docIdList;
    }

    /**
     * 列表去重
     * @param segTokenList
     * @return
     */
    public Set<SegToken> removeDuplicate(List<SegToken> segTokenList){
        Set<SegToken> set = new TreeSet<SegToken>(new Comparator<SegToken>() {
            @Override
            public int compare(SegToken o1, SegToken o2) {
                //字符串,则按照asicc码升序排列
                return o1.word.compareTo(o2.word);
            }
        });
        set.addAll(segTokenList);
        return set;
    }

    public List<DataBO> orderByCos(HashSet<Long> docIdSet,Integer pageNo,Integer pageSize,String queryContent,Boolean returnALlFlag){
        List<DataBO> dataBOList = selectByDocId(docIdSet);
        //将内容文本解析出来存放到List<String>列表中
//        List<String> documentList = new ArrayList<>();
//        for (DataBO dataBO : dataBOList) {
//            documentList.add(dataBO.getWeiboContent());
//        }
        Map<String, Double> similarMap = GetCosMap.caculate(dataBOList, queryContent);
//        System.out.println("similarMap数据："+similarMap.toString());
        ArrayList<Map.Entry<String, Double>> list = sortMap(similarMap);
        List<DataBO> list1 = getList(list);

        if(returnALlFlag) {
            return list1;
        }else{
            int lastIndex=pageNo*pageSize+pageSize;
            if(lastIndex>=list1.size()){
                lastIndex=list1.size();
            }
            if(pageNo*pageSize<=lastIndex){
                return list1.subList(pageNo*pageSize,lastIndex);
            }else{
                ArrayList<DataBO> dataBOS = new ArrayList<>();
                return dataBOS;
            }
        }

    }

    public static ArrayList<Map.Entry<String, Double>> sortMap(Map<String,Double> map){
        ArrayList<Map.Entry<String, Double>> list = new ArrayList<>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            //降序
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        return list;
    }

    public List<DataBO> getList(ArrayList<Map.Entry<String, Double>> list){
        List<DataBO> dataBOList = new ArrayList<>();
        for (Map.Entry<String, Double> map : list) {
            DataPO dataPO = new DataPO();
            dataPO.setId(Long.valueOf(map.getKey()));
            DataPO detailByContent = rawDataMapper.getDetailById(dataPO);
            DataBO dataBO = new DataBO();
            BeanUtils.copyProperties(detailByContent,dataBO);
            dataBOList.add(dataBO);
        }
        return dataBOList;
    }
}
