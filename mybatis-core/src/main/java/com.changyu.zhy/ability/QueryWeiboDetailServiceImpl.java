package com.changyu.zhy.ability;

import com.changyu.zhy.bo.DataBO;
import com.changyu.zhy.dao.RawDataMapper;
import com.changyu.zhy.po.DataPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 描述：根据微博id查询详情
 *
 * @author zhanghaoyu
 * @date 2019/5/1 21:10
 **/
@Service("queryWeiBoDetailService")
public class QueryWeiboDetailServiceImpl implements QueryWeiBoDetailService{
    @Autowired
    private RawDataMapper rawDataMapper;
    @Override
    public DataBO queryWeiboByDocId(DataBO dataBO) {
        DataPO dataPO = new DataPO();
        BeanUtils.copyProperties(dataBO,dataPO);
        DataPO detail = rawDataMapper.getDetailById(dataPO);
        BeanUtils.copyProperties(detail,dataBO);
        return dataBO;
    }
}
