package com.changyu.zhy.mq.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 描述：消息生产者测试实例
 *
 * @author zhanghaoyu
 * @date 2018/12/7 20:37
 **/
@Slf4j
public class RocketMqTestProducer {
    /*@Autowired
    private DefaultMQProducer defaultMQProducer;

    *//**
     * 发送消息
     *//*
    public void send(){
        log.debug("消息生产者开始发送消息");
        String messageStr="测试消息";
        Message message = new Message("TEST_CONSUMER_TOPIC", "TEST_CONSUMER_TAG", messageStr.getBytes());
        SendResult send=null;
        try{
            send = defaultMQProducer.send(message);
        }catch (Exception e){
            log.error("消息生产者测试消息发送失败");
        }
        log.debug("消息生产者发送消息结束");
    }*/
}
