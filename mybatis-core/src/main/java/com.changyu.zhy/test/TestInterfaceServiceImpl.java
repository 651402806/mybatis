package com.changyu.zhy.test;

import com.changyu.zhy.bo.BlogtypeBO;
import com.changyu.zhy.dao.BlogTypeMapper;
import com.changyu.zhy.exception.BusinessException;
import com.changyu.zhy.mq.producer.RocketMqTestProducer;
import com.changyu.zhy.po.BlogTypePO;
import com.changyu.zhy.bo.TestInterfaceReqBO;
import com.changyu.zhy.bo.TestInterfaceRspBO;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/4 15:17
 **/
@Service("testInterfaceService")
public class TestInterfaceServiceImpl implements TestInterfaceService {
    private static final Logger log=LoggerFactory.getLogger(TestInterfaceServiceImpl.class);
    @Autowired
    private BlogTypeMapper blogTypeMapper;
    @Autowired
    private TestAtomService testAtomService;
    @Autowired
    private DefaultMQProducer defaultMQProducer;
    @Autowired
    private DefaultMQPushConsumer defaultMQPushConsumer;

    /**
     * 注入配置值
     */
    @Value("${rocketmq.topic.test}")
    private String topic;
    @Value("${rocketmq.tag.test}")
    private String tag;

    @Override
    public TestInterfaceRspBO test(TestInterfaceReqBO testInterfaceReqBO){
        verifyParams(testInterfaceReqBO);
        BlogTypePO blogTypePO = new BlogTypePO();
        BeanUtils.copyProperties(testInterfaceReqBO,blogTypePO);
        /*int insert = blogTypeMapper.insert(blogTypePO);
        TestInterfaceRspBO testInterfaceRspBO = new TestInterfaceRspBO();
        testInterfaceRspBO.setId(blogTypePO.getId());
        return testInterfaceRspBO;*/
        List<BlogTypePO> select = blogTypeMapper.select(blogTypePO);
        ArrayList<BlogtypeBO> blogtypeBOList = new ArrayList<>();
        for (BlogTypePO typePO : select) {
            BlogtypeBO blogtypeBO = new BlogtypeBO();
            BeanUtils.copyProperties(typePO,blogtypeBO);
            blogtypeBOList.add(blogtypeBO);
        }
        TestInterfaceRspBO testInterfaceRspBO = new TestInterfaceRspBO();
        testInterfaceRspBO.setBlogtypeBOList(blogtypeBOList);
        testAtomService.testAtom(testInterfaceReqBO);
        //测试消息发送者
        log.debug("消息生产者开始发送消息");
        String messageStr="测试消息";
        SendResult send=null;
        try{
            Message message = new Message(topic, tag, messageStr.getBytes("UTF-8"));
            //延迟消息，延迟消息的生效时间，使其在某一特定级别的时间段后生效,当前设置级别3，时间为10分钟
            //message.setDelayTimeLevel(3);
            send = defaultMQProducer.send(message);
//            defaultMQProducer.start();
        }catch (Exception e){
            log.error("消息生产者测试消息发送失败");
        }
        log.debug("消息生产者发送消息结束");
        testInterfaceRspBO.setResponseCode("0000");
        testInterfaceRspBO.setResponseDesc("测试描述");
        return testInterfaceRspBO;
    }

    /**
     * 参数校验
     */
    private void verifyParams(TestInterfaceReqBO testInterfaceReqBO){
        if(testInterfaceReqBO==null){
            throw new BusinessException("8888","入参不能为空");
        }
        if(testInterfaceReqBO.getTypeName()==null){
            throw new BusinessException("8888","入参参数[typeName]不能为空");
        }
    }
}
