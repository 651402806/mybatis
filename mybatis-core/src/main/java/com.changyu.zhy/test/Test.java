package com.changyu.zhy.test;

import com.changyu.zhy.bo.DataBO;
import com.changyu.zhy.util.GetCosMap;
import com.changyu.zhy.util.StopWord;
import com.changyu.zhy.util.TfIdf;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.mining.word.WordInfo;
import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.SegToken;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 14:35
 **/
public class Test {
//    public static void main(String[] args) {
//        System.out.println(getCount("、","我的世界世界是我的"));
//    }
//
//    public static Integer getCount(String word,String content) {
//        int count = 0;
//        word=word.replaceAll("[^a-zA-Z0-9\\u4E00-\\u9FA5]", "");
//        Pattern p = Pattern.compile(word, Pattern.CASE_INSENSITIVE);
//        Matcher m = p.matcher(content);
//        while (m.find()) {
//            count++;
//        }
//        return count;
//    }
    public static void main(String[] args) {
        String content = "绝地求生和先生2014年结婚，2016年宝宝出生绝地求生，2017年我回老家给宝宝断奶，先生和一个外地供应商发生一夜澡友情。女方怀孕，我们去找她，她答应打掉孩子。然而在我们都不知道的情况下她居然把孩子生了下来，目前大八个月。我知道后坚决离婚，考虑到孩子，考虑到跟先生平时相处还行，我内心真的很纠结，要不要离婚？绝地求生#绝地求生# 犹豫就会败北，果断等于白给！ 托马斯“我就是白给”狗贼带萌新水友打决赛圈，把打包的任务交给水友，自己去冲锋（白给），水友成功带狗托吃鸡！\uE627绝地求生精彩击杀 LLemon_视频的微博视频";
//        List<String> keywordList = HanLP.extractKeyword(content, 5);
//        System.out.println(keywordList);
        List<WordInfo> wordInfos = HanLP.extractWords(content, 100);
        System.out.println(wordInfos.toString());
    }
}
