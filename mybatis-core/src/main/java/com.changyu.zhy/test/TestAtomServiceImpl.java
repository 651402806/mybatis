package com.changyu.zhy.test;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.changyu.zhy.bo.TestInterfaceReqBO;
import com.changyu.zhy.bo.TestInterfaceRspBO;
import com.changyu.zhy.dao.BlogTypeMapper;
import com.changyu.zhy.po.BlogTypePO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：mybatis plus分页插件测试
 *
 * @author zhanghaoyu
 * @date 2018/12/4 19:55
 **/
@Service("testAtomService")
@Slf4j
public class TestAtomServiceImpl implements TestAtomService{

    @Autowired
    private BlogTypeMapper blogTypeMapper;
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Override
    public TestInterfaceRspBO testAtom(TestInterfaceReqBO testInterfaceReqBO) {
        Page<BlogTypePO> blogTypePOPage = new Page<>(1,1);
        //第二个参数为sql查询条件可为空
        IPage<BlogTypePO> blogTypePOIPage = blogTypeMapper.selectPage(blogTypePOPage, null);
        List<BlogTypePO> blogTypePOList = blogTypePOIPage.getRecords();
        log.debug("page:"+String.valueOf(blogTypePOList));
        ValueOperations<String,Object> valueOperations = redisTemplate.opsForValue();
        valueOperations.set("2","3");
        log.debug("redis测试数据"+redisTemplate.opsForValue().get("2").toString());
        return null;
    }
}
