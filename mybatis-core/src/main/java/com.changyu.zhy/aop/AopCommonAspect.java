package com.changyu.zhy.aop;

import com.alibaba.fastjson.JSON;
import com.changyu.zhy.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;


/**
 * 描述：通用环绕切面
 *
 * @author zhanghaoyu
 * @date 2018/12/6 11:42
 **/
@Slf4j
public class AopCommonAspect {
    /**
     * @param proceedingJoinPoint
     * @description 需要将切入点的被代理对象的返回值返回，否则切面环绕后被代理方法无法正常返回
     */
    public Object around(ProceedingJoinPoint proceedingJoinPoint) {
        //日志打印入参json
        logReqArgs(proceedingJoinPoint);
        Object rspBO = null;
        try {
            rspBO = proceedingJoinPoint.proceed();
        } catch (BusinessException businessException) {
            //封装异常出参
            rspBO = packageRspBO(proceedingJoinPoint, businessException);
            log.error("异常:\n"+ businessException.getResponseDesc());
        } catch (Throwable e) {
            log.error("异常:\n",e);
        }
        //日志打印出参json
        logPrintJson(proceedingJoinPoint,rspBO,"出参");
        return rspBO;
    }

    /**
     * @param proceedingJoinPoint
     * @decription 获取切入点类的方法
     */
    public Method getMethod(ProceedingJoinPoint proceedingJoinPoint) {
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = methodSignature.getMethod();
        return method;
    }

    /**
     * @param proceedingJoinPoint 切入点
     * @param businessException   异常
     * @description 封装出参异常描述
     */
    public Object packageRspBO(ProceedingJoinPoint proceedingJoinPoint, BusinessException businessException) {
        Method method = getMethod(proceedingJoinPoint);
        Class<?> rspBO = method.getReturnType();
        Object object = null;
        try {
            object = rspBO.newInstance();
            Method setResponseCode = rspBO.getMethod("setResponseCode",String.class);
            Method setResponseDesc = rspBO.getMethod("setResponseDesc",String.class);
            if (setResponseCode != null) {
                setResponseCode.invoke(object, businessException.getResponseCode());
            } else {
                log.error("该类没有setResponseCode(String)方法存在\n");
            }
            if (setResponseDesc != null) {
                setResponseDesc.invoke(object, businessException.getResponseDesc());
            } else {
                log.error("该类没有setResponseDesc(String)方法存在\n");
            }
        } catch (Throwable e) {
            log.error("实例化出参对象失败:\n",e);
        }
        return object;
    }

    /**
     * 日志打印
     * @param proceedingJoinPoint 切入点方法
     * @param object 出入参
     */
    public void logPrintJson(ProceedingJoinPoint proceedingJoinPoint,Object object,String paramType){
        //获取当前对象的类名
        String className = proceedingJoinPoint.getTarget().getClass().getName();
        //获取当前执行方法的方法名
        Method method = getMethod(proceedingJoinPoint);
        String methodName = method.getName();
        //输出按json的缩进输出
        String jsonString = JSON.toJSONString(object, true);
        log.debug("\n"+paramType+"\n类名:"+className+"\n方法名:"+methodName+"\n"+jsonString+"\n");
    }

    /**
     * 打印入参日志
     * @param proceedingJoinPoint
     */
    public void logReqArgs(ProceedingJoinPoint proceedingJoinPoint){
        Object[] args = proceedingJoinPoint.getArgs();
        logPrintJson(proceedingJoinPoint,args,"入参");
    }
}
