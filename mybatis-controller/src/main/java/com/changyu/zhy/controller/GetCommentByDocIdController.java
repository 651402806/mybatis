package com.changyu.zhy.controller;

import com.changyu.zhy.bo.CommentTableBO;
import com.changyu.zhy.bo.GetCommentByDocIdReqBO;
import com.changyu.zhy.nlp.GetCommentByDocIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/30 20:53
 **/
@RestController
@CrossOrigin(origins = {"*"}, maxAge = 3600)
@RequestMapping("/query")
public class GetCommentByDocIdController {
    @Autowired
    private GetCommentByDocIdService getCommentByDocIdService;
    @PostMapping("getComment")
    public List<CommentTableBO> getCommentList(GetCommentByDocIdReqBO commentByDocIdReqBO){
        return getCommentByDocIdService.findCommentByDocId(commentByDocIdReqBO);
    }
}
