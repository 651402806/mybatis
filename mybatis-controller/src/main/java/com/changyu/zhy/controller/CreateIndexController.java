package com.changyu.zhy.controller;

import com.changyu.zhy.nlp.CreateInvertedIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 14:22
 **/
@RestController
@RequestMapping("/index")
public class CreateIndexController {
    @Autowired
    private CreateInvertedIndexService createInvertedIndexService;
    @PostMapping("create")
    public void createInvertedIndexService(){
        createInvertedIndexService.createInvertedIndex();
    }
}
