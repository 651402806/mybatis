package com.changyu.zhy.controller;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.changyu.zhy.bo.DataBO;
import com.changyu.zhy.bo.QueryReqBO;
import com.changyu.zhy.nlp.SearchByQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/5/12 21:43
 **/
@RestController
@CrossOrigin(origins = {"*"}, maxAge = 3600)
@RequestMapping("/query")
public class ExportExcelController {
    @Autowired
    private SearchByQueryService searchByQueryService;
    @GetMapping("exportExcel")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response,String queryContent){
        System.out.println(queryContent);
        try {
            ServletOutputStream out = response.getOutputStream();
            ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
            String temp=queryContent;
            String fileName=java.net.URLEncoder.encode(temp, "UTF-8")+" "+new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            Sheet sheet1 = new Sheet(1, 0);
            sheet1.setSheetName(queryContent);
            writer.write0(getData(queryContent), sheet1);
            response.setContentType("multipart/form-data");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition", "attachment;filename="+ fileName+".xlsx");
            writer.finish();
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取当前关键词所有结果
     */
    public List<List<String>> getData(String queryContent){
        QueryReqBO queryReqBO = new QueryReqBO();
        queryReqBO.setReturnAllFlag(true);
        queryReqBO.setQueryContent(queryContent);
        List<DataBO> query = searchByQueryService.query(queryReqBO);
        List<List<String>> wrap = new ArrayList<>();
        for (DataBO dataBO : query) {
            List<String> row = new ArrayList<>();
            row.add(dataBO.getWeiboContent());
            row.add(dataBO.getTitle());
            row.add(dataBO.getWeiboDate());
            row.add(dataBO.getUrl());
            row.add(dataBO.getWeiboUser());
            row.add(dataBO.getCommentNum().toString());
            row.add(dataBO.getWeiboPraised().toString());
            wrap.add(row);
        }
        return  wrap;
    }
}
