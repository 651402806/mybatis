package com.changyu.zhy.controller;

import com.changyu.zhy.ability.QueryWeiBoDetailService;
import com.changyu.zhy.bo.DataBO;
import com.changyu.zhy.nlp.GetKeyWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/5/1 21:18
 **/
@RestController
@CrossOrigin(origins = {"*"}, maxAge = 3600)
@RequestMapping("/query")
public class QueryDetailByDocId {
    @Autowired
    private QueryWeiBoDetailService queryWeiBoDetailService;
    @Autowired
    private GetKeyWordService getKeyWordService;
    @GetMapping("detail")
    public DataBO queryDetailByDocId(DataBO dataBO){
        return queryWeiBoDetailService.queryWeiboByDocId(dataBO);
    }
    @PostMapping("getKeyWord")
    public List<Map<String, Object>> getTopNKeyWord(Integer topN, String content){
        return getKeyWordService.getTopNKeyWord(topN,content);
    }
}
