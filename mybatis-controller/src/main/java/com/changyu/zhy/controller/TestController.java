package com.changyu.zhy.controller;

import com.changyu.zhy.bo.*;
import com.changyu.zhy.nlp.SearchByQueryService;
import com.changyu.zhy.nlp.SentimentSearchService;
import com.changyu.zhy.test.TestAtomService;
import com.changyu.zhy.test.TestInterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/4 15:23
 **/
@RestController
@CrossOrigin(origins = {"*"}, maxAge = 3600)
@RequestMapping("/test")
public class TestController {
//    @Autowired
//    private TestAtomService testAtomService;
//    @Autowired
//    private TestInterfaceService testInterfaceService;
//    @PostMapping("insert")
//    public TestInterfaceRspBO insert(TestInterfaceReqBO testInterfaceReqBO){
//        TestInterfaceRspBO test = testInterfaceService.test(testInterfaceReqBO);
//        return test;
//    }
    @Autowired
    private SearchByQueryService searchByQueryService;
    @Autowired
    private SentimentSearchService sentimentSearchService;
    @PostMapping("query")
    public List<DataBO> query(QueryReqBO queryReqBO){
        return searchByQueryService.query(queryReqBO);
    }
    @PostMapping("getCount")
    public SentimentCountRspBO getCount(SentimentCountReqBO sentimentCountReqBO){
        return sentimentSearchService.getSentimentCount(sentimentCountReqBO);
    }
}
