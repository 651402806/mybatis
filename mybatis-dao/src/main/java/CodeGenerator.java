import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * 描述：mybatis plus代码生成器配置
 *
 * @author zhanghaoyu
 * @date 2018/12/10 16:37
 **/
public class CodeGenerator {
    /**
     * 模板配置信息
     */
    public static String PROPERTIES_FILE="templates/templates-config.properties";

    /**
     * 作者
     */
    public static String AUTHOR="";
    /**
     * 项目下module名
     */
    public static String MODULENAME="";
    /**
     * 模块中的输出路径
     */
    public static String PATH="";
    /**
     * 数据库url
     */
    public static String URL="";
    /**
     * 驱动包名
     */
    public static String DRIVERNAME="";
    /**
     * 数据库用户名
     */
    public static String USERNAME="";
    /**
     * 数据库密码
     */
    public static String PASSWORD="";
    /**
     * 模块下父包名
     */
    public static String PARENT="";
    /**
     * 数据库表名以逗号分隔
     */
    public static String TABLElIST="";
    /**
     * 模板在resource资源目录下的文件夹路径
     */
    public static String TEMPLATES_ROOT_PATH="";
    /**
     * mapper xml文件模板
     */
    public static String MAPPER_XML_TEMPLATE="";
    /**
     * po java模板
     */
    public static String PO_JAVA_TEMPLATE="";
    /**
     * mapper接口模板
     */
    public static String MAPPER_JAVA_TEMPLATE="";
    /**
     * bo模板
     */
    public static String BO_JAVA_TEMPLATE="";
    /**
     * bo生成的模块名
     */
    public static String BO_MODULE_NAME="";
    /**
     * bo生成的文件夹路径默认src/main/java
     */
    public static String BO_PATH="";
    /**
     * bo所处的父包名
     */
    public static String BO_PARENT="";
    /**
     * mapper xml生成包名
     */
    public static String MAPPER_XML_PACKAGENAME="";
    /**
     * mapper接口生成包名
     */
    public static String MAPPER_JAVA_PACKAGENAME="";
    /**
     * po java生成包名
     */
    public static String PO_JAVA_PACKAGENAME="";
    /**
     * BO java生成包名
     */
    public static String BO_JAVA_PACKAGENAME="";

    /**
     * 读取配置文件
     */
    public static void readProperties(){
       FileInputStream fileInputStream=null;
        try {
            ClassLoader classLoader = CodeGenerator.class.getClassLoader();
            URL resource = classLoader.getResource(PROPERTIES_FILE);
            String path = resource.getPath();
            Properties properties = new Properties();
            fileInputStream = new FileInputStream(path);
            properties.load(fileInputStream);
            packageParam(properties);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 组装配置信息
     */
    public static void packageParam(Properties properties){
        AUTHOR=properties.getProperty("author");
        MODULENAME=properties.getProperty("moduleName");
        PATH=properties.getProperty("path","src/main/java");
        URL=properties.getProperty("url");
        DRIVERNAME=properties.getProperty("driverName","com.mysql.jdbc.Driver");
        USERNAME=properties.getProperty("userName");
        PASSWORD=properties.getProperty("password");
        PARENT=properties.getProperty("parent");
        TABLElIST=properties.getProperty("tableList");
        TEMPLATES_ROOT_PATH=properties.getProperty("templatesRootPath","templates");
        MAPPER_XML_TEMPLATE=properties.getProperty("mapper.xml.template");
        PO_JAVA_TEMPLATE=properties.getProperty("po.java.template");
        MAPPER_JAVA_TEMPLATE=properties.getProperty("mapper.java.template");
        BO_JAVA_TEMPLATE=properties.getProperty("bo.java.template","mapper.java.ftl");
        BO_MODULE_NAME=properties.getProperty("bo.moduleName",MODULENAME);
        BO_PATH=properties.getProperty("bo.path","src/main/java");
        BO_PARENT=properties.getProperty("bo.parent");
        MAPPER_XML_PACKAGENAME=properties.getProperty("mapper.xml.packageName","mappers");
        MAPPER_JAVA_PACKAGENAME=properties.getProperty("mapper.java.packageName","dao");
        PO_JAVA_PACKAGENAME=properties.getProperty("po.java.packageName","po");
        BO_JAVA_PACKAGENAME=properties.getProperty("bo.java.packageName","bo");
    }

    /**
     * 切分表名
     */
    public static String[] splitTableList(){
        return  TABLElIST.split(",");
    }

    /**
     * 路径连接
     */
    public static String concatToPath(String...pathElement){
        String absoultePath="";
        for (String element : pathElement) {
            absoultePath=absoultePath+"/"+element;
        }
        return absoultePath;
    }

    /**
     * 将包名转换为路径
     */
    public static String packageToPath(String packageName){
        String[] split = packageName.split("\\.");
        return concatToPath(split).substring(1);
    }

    /**
     * 在指定路径下输出mapper接口，mapper.xml,po.java
     * @return
     */
    public static InjectionConfig daoFileOutput(String projectPath){
        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                //封装到模板引擎使用的map
                Map<String, Object> map = new HashMap<>(16);
                map.put("boPackage",BO_PARENT+"."+BO_JAVA_PACKAGENAME);
                map.put("poPackage",PARENT+"."+PO_JAVA_PACKAGENAME);
                map.put("mapper",PARENT+"."+MAPPER_JAVA_PACKAGENAME);
                map.put("mapperXml",PARENT+"."+MAPPER_XML_PACKAGENAME);
                setMap(map);
            }
        };
        List<FileOutConfig> focList = new ArrayList<>();

        //定义mapper.xml的文本模板和输出位置
        focList.add(new FileOutConfig(concatToPath(TEMPLATES_ROOT_PATH,MAPPER_XML_TEMPLATE)) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件路径
                String relativePath = concatToPath( MODULENAME, PATH, packageToPath(PARENT), MAPPER_XML_PACKAGENAME, tableInfo.getXmlName()+ StringPool.DOT_XML);
                return projectPath+relativePath;
            }
        });

        //定义mapper接口的文本模板位置和输出位置
        focList.add(new FileOutConfig(concatToPath(TEMPLATES_ROOT_PATH,MAPPER_JAVA_TEMPLATE)) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                String relativePath = concatToPath( MODULENAME, PATH, packageToPath(PARENT), MAPPER_JAVA_PACKAGENAME, tableInfo.getMapperName()+StringPool.DOT_JAVA);
                return projectPath+relativePath;
            }
        });

        //定义po.java的文本模板位置和输出位置
        focList.add(new FileOutConfig(concatToPath(TEMPLATES_ROOT_PATH,PO_JAVA_TEMPLATE)) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                String relativePath = concatToPath( MODULENAME, PATH, packageToPath(PARENT), PO_JAVA_PACKAGENAME, tableInfo.getEntityName()+StringPool.DOT_JAVA);
                return projectPath+relativePath;
            }
        });

        //定义Bo.java的文本模板位置和输出位置
        focList.add(new FileOutConfig(concatToPath(TEMPLATES_ROOT_PATH,BO_JAVA_TEMPLATE)) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                String relativePath = concatToPath( BO_MODULE_NAME, BO_PATH, packageToPath(BO_PARENT), BO_JAVA_PACKAGENAME, tableInfo.getControllerName()+StringPool.DOT_JAVA);
                return projectPath+relativePath;
            }
        });
        cfg.setFileOutConfigList(focList);
        return cfg;
    }

    public static void main(String[] args) {
        //读取配置文件
        readProperties();
        AutoGenerator autoGenerator = new AutoGenerator();
        //全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        //获取项目所在根路径，不包含模块路径
        String projectPath = System.getProperty("user.dir");
        //设置作者
        globalConfig.setAuthor(AUTHOR);
        //设置输出路径
        globalConfig.setOutputDir(concatToPath(projectPath,MODULENAME,PATH));
        //是否覆盖同名文件
        globalConfig.setFileOverride(true);
        //不打开生成的文件
        globalConfig.setOpen(false);
        globalConfig.setBaseColumnList(true);
        globalConfig.setBaseResultMap(true);

        //设置实体类名称的命名形式
        globalConfig.setEntityName("%sPO");
        //因缺乏BO全局设置，且中途切换实体类命名失败，顾采用controllerName代替BOName
        globalConfig.setControllerName("%sBO");

        autoGenerator.setGlobalConfig(globalConfig);

        //数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setUrl(URL);
        dataSourceConfig.setDriverName(DRIVERNAME);
        dataSourceConfig.setUsername(USERNAME);
        dataSourceConfig.setPassword(PASSWORD);
        autoGenerator.setDataSource(dataSourceConfig);

        //包配置
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent(PARENT);
        autoGenerator.setPackageInfo(packageConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        //表名下划线转驼峰
        strategy.setNaming(NamingStrategy.underline_to_camel);
        //字段名下划线转驼峰
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        //使用lombok插件代替get set
        strategy.setEntityLombokModel(true);
        //需要生成的表
        strategy.setInclude(splitTableList());
        autoGenerator.setStrategy(strategy);
        InjectionConfig injectionConfig = daoFileOutput(projectPath);
        autoGenerator.setCfg(injectionConfig);

        //取消默认模板位置生成
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setXml(null);
        templateConfig.setServiceImpl(null);
        templateConfig.setMapper(null);
        templateConfig.setEntityKt(null);
        templateConfig.setService(null);
        templateConfig.setController(null);
        templateConfig.setEntity(null);
        autoGenerator.setTemplate(templateConfig);

        //模板引擎
        autoGenerator.setTemplateEngine(new FreemarkerTemplateEngine());
        autoGenerator.execute();
    }
}
