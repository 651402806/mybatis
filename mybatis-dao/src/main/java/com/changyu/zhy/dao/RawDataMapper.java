package com.changyu.zhy.dao;

import com.changyu.zhy.po.DataPO;
import org.apache.ibatis.annotations.Param;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 20:19
 **/
public interface RawDataMapper {
    /**
     *
     * @param docIdList
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<DataPO> getListPage(Map<String,Object> queryMap);
    DataPO getDetailById(DataPO dataPO);
    DataPO getDetailByContent(DataPO dataPO);
    List<DataPO> getAll();
}
