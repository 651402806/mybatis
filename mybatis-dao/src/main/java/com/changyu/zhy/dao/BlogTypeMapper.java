package com.changyu.zhy.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.changyu.zhy.po.BlogTypePO;

import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/4 14:38
 * @description PO需要关联表名
 **/
public interface BlogTypeMapper extends BaseMapper<BlogTypePO> {
    int insert(BlogTypePO blogTypePO);
    List<BlogTypePO> select(BlogTypePO blogTypePO);
}
