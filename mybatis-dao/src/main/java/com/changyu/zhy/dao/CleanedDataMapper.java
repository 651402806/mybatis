package com.changyu.zhy.dao;

import com.changyu.zhy.po.DataPO;

import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 14:11
 **/
public interface CleanedDataMapper {
    List<DataPO> getList(DataPO dataPO);
}
