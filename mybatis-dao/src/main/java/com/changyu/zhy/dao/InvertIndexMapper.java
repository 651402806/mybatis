package com.changyu.zhy.dao;

import com.changyu.zhy.po.DataPO;
import com.changyu.zhy.po.InvertIndexPO;

import java.util.List;
import java.util.Map;

/**
 * 描述：倒排索引表
 *
 * @author zhanghaoyu
 * @date 2019/4/23 16:13
 **/
public interface InvertIndexMapper {
    /**
     * 插入倒排索引数据
     * @param invertIndexPOList
     * @return
     */
    void insertIndex(List<InvertIndexPO> invertIndexPOList);

    /**
     * 根据查询条件查询倒排索引
     * @param invertIndexPO
     * @return
     */
    List<InvertIndexPO> getList(InvertIndexPO invertIndexPO);
}
