package com.changyu.zhy.dao;


import com.changyu.zhy.po.CommentTablePO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface CommentTableMapper {

	/**
	 * 1.insert新增一条数据
	 * 注: 根据Model实体执行新增操作.
	 * @param commentTablePO
	 * @return int
	 * @throws Exception
	 */
	int insert(CommentTablePO commentTablePO);

	/**
	 * 2.deleteById根据主键ID删除一条数据
	 * 注: 根据主键ID执行删除操作.
	 * @param id
	 * @return int
	 * @throws Exception
	 */
	int deleteById(long id);

	/**
	 * 3.deleteByIds根据主键ID批量删除数据
	 * 注: 根据拼接有限个主键ID执行多条数据的删除操作.
	 * @param id
	 * @return int
	 * @throws Exception
	 */
	int deleteByIds(int[] ids);

	/**
	 * 4.deleteBy根据Model删除数据
	 * 注: 根据Model拼接条件执行删除操作.
	 * @param commentTablePO
	 * @return int
	 * @throws Exception
	 */
	int deleteBy(CommentTablePO commentTablePO);

	/**
	 * 5.updateById根据Model实体ID更新一条数据
	 * 注: 根据Model实体ID更新一条数据.
	 * @param commentTablePO
	 * @return int
	 * @throws Exception
	 */
	int updateById(CommentTablePO commentTablePO);

	/**
	 * 6.getModelById根据ID获取一个Model实体
	 * 注: 根据主键ID获取一个Model实体.
	 * @param id
	 * @return TablePO
	 * @throws Exception
	 */
	CommentTablePO getModelById(long id);

	/**
	 * 7.getModelBy根据Model获取一个Model实体
	 * 注: 根据Model实体获取一个Model实体.
	 * @param commentTablePO
	 * @return TablePO
	 * @throws Exception
	 */
	CommentTablePO getModelBy(CommentTablePO commentTablePO);

	/**
	 * 8.getList条件查询，不分页
	 * 注: 支持多条件查询、模糊查询、日期比较查询等操作.
	 * @param commentTablePO
	 * @return List<TablePO>
	 * @throws Exception
	 */
	List<CommentTablePO> getList(CommentTablePO commentTablePO);

	/**
	 * 9.getListPage条件查询，分页
	 * 注: 支持多条件查询、模糊查询、日期比较查询等操作.
	 * @return List<TablePO>
	 * @throws Exception
	 */
	List<CommentTablePO> getListPage(Map<String,Object> map);

	/**
	 * 10.getCheckById根据主键ID验证一条数据是否存在
	 * 注: 根据主键ID验证该数据是否存在.
	 * @param id
	 * @return int
	 * @throws Exception
	 */
	int getCheckById(long id);

	/**
	 * 11.getCheckBy根据Model实体验证一条数据是否存在
	 * 注: 根据Model实体验证该数据是否存在.
	 * @param commentTablePO
	 * @return int
	 * @throws Exception
	 */
	int getCheckBy(CommentTablePO commentTablePO);

	/**
	 * 12.insertBatch根据List对象批量新增数据
	 * 注: 根据List对象执行批量新增操作.
	 * @param list
	 * @throws Exception
	 */
	void insertBatch(List<CommentTablePO> list);

	/**
	 * 查询数量
	 */
	int getCount(CommentTablePO commentTablePO);
}