package com.changyu.zhy.po;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 14:08
 **/
@Data
public class DataPO implements Serializable {
    /**
     * 微博id
     */
    private Long id;
    /**
     * 微博文本
     */
    private String weiboContent;
    /**
     * 微博发布者
     */
    private String weiboUser;
    /**
     * 微博爬取时间
     */
    private Date createTime;
    /**
     * 微博标题
     */
    private String title;
    /**
     * 访问地址
     */
    private String url;
    /**
     * 微博点赞数
     */
    private Long weiboPraised;
    /**
     * 微博发布日期
     */
    private String weiboDate;
    /**
     * 评论数
     */
    private Long commentNum;

}
