package com.changyu.zhy.po;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/4 14:35
 * @describe 指定表名，接口泛型继承的时候会传入PO对应的表名，然后使用插件的CRUD方法，不写sql
 **/
@Data
@TableName("blogtypes")
public class BlogTypePO implements Serializable {
    private static final long serialVersionUID = -2698869296948370822L;
    private String id;
    private String typeName;
}
