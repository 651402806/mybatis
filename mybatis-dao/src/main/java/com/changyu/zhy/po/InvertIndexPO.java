package com.changyu.zhy.po;

import lombok.Data;

import java.io.Serializable;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2019/4/23 16:11
 **/
@Data
public class InvertIndexPO implements Serializable {
    /**
     * id
     */
    private Long id;
    /**
     * 单词
     */
    private String word;
    /**
     * 文档id
     */
    private Long docId;
    /**
     * 文档词频
     */
    private Integer tf;
}
