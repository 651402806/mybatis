package com.changyu.zhy.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommentTablePO implements Serializable {

	/**
	 * 评论id
	 */
	private Long id;
	/**
	 * 微博id
	 */
	private Long weiboId;
	/**
	 * 用户名
	 */
	private String userId;
	/**
	 * 评论内容
	 */
	private String commentText;
	/**
	 * 评论情感
	 */
	private String emotion;
	/**
	 * 评论情感极性
	 */
	private Integer emotionalPolarity;
	/**
	 * 评论日期
	 */
	private String commentDate;
}