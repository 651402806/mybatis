package ${cfg.mapper};

import ${cfg.poPackage}.${entity};
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <p>
 * ${table.comment!} Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
public interface ${table.mapperName}{
    /**
	 * 1.insert新增一条数据
	 * 注: 根据Model实体执行新增操作.
	 * @param ${entity?uncap_first}
	 */
	 int insert(${entity} ${entity?uncap_first});

    /**
	 * 2.deleteById根据主键ID删除一条数据
	 * 注: 根据主键ID执行删除操作.
	 * @param id
	 * @return int
	 */
	 int deleteById(long id);

    /**
	 * 4.deleteBy根据Model删除数据
	 * 注: 根据Model拼接条件执行删除操作.
	 * @param ${entity?uncap_first}
	 * @return int
	 */
	 int deleteBy(${entity} ${entity?uncap_first});

    /**
	 * 5.updateById根据Model实体ID更新一条数据
	 * 注: 根据Model实体ID更新一条数据.
	 * @param ${entity?uncap_first}
	 * @return int
	 */
	 int updateById(${entity} ${entity?uncap_first});

    /**
	 * 6.getModelById根据ID获取一个Model实体
	 * 注: 根据主键ID获取一个Model实体.
	 * @param id
	 * @return ${entity}
	 */
     ${entity} getModelById(long id);

    /**
	 * 7.getModelBy根据Model获取一个Model实体
	 * 注: 根据Model实体获取一个Model实体.
	 * @param ${entity?uncap_first}
	 * @return ${entity}
	 */
     ${entity} getModelBy(${entity} ${entity?uncap_first});

	/**
	 * 8.getList条件查询，不分页
	 * 注: 支持多条件查询、模糊查询、日期比较查询等操作.
	 * @param ${entity?uncap_first}
	 * @return list
	 */
	 List<${entity}> getList(${entity} ${entity?uncap_first});

    /**
    * 9.getListPage条件查询，分页
    * 注: 支持多条件查询、模糊查询、日期比较查询等操作.
    * @param ${entity?uncap_first}
    * @return list
    */
    List<${entity}> getListPage(${entity} ${entity?uncap_first}, Page<${entity}> page);
}
