<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${cfg.mapper}.${table.mapperName}">

<#if baseResultMap>
    <!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="${cfg.poPackage}.${entity}">
<#list table.fields as field>
<#if field.keyFlag><#--生成主键排在第一位-->
        <id column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
<#list table.commonFields as field><#--生成公共字段 -->
    <result column="${field.name}" property="${field.propertyName}" />
</#list>
<#list table.fields as field>
<#if !field.keyFlag><#--生成普通字段 -->
        <result column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
    </resultMap>

</#if>
<#if baseColumnList>
    <!-- 通用查询结果列 -->
    <sql id="Base_Column_List">
<#list table.commonFields as field>
        ${field.name},
</#list>
        ${table.fieldNames}
    </sql>

</#if>
    <!--
	**************************************************************************
	* insert新增一条数据
	* 注: 根据Model实体执行新增操作.
	**************************************************************************
	-->
    <insert id="insert"
            parameterType="${cfg.poPackage}.${entity}"
            useGeneratedKeys="true"
            keyProperty="id">
        INSERT INTO
            ${table.name}
            (
        <#list table.fields as field>
        <#if field_index==0>
             ${field.name}
        <#else>
            ,${field.name}
        </#if>
        </#list>
            )
        VALUES
            (
        <#list table.fields as field>
        <#if field_index==0>
            ${'#'}{${field.propertyName}}
        <#else>
            ,${'#'}{${field.propertyName}}
        </#if>
        </#list>
            )
    </insert>

    <!--
	**************************************************************************
	* deleteById根据主键ID删除一条数据
	* 注: 根据主键ID执行删除操作.
	**************************************************************************
	-->
    <delete id="deleteById"
            parameterType="long">

        DELETE
        FROM  ${table.name}
        WHERE    ${table.fields[0].name} =${'#'}{${table.fields[0].propertyName}}
    </delete>

    <!--
	**************************************************************************
	* deleteBy根据Model删除数据
	* 注: 根据Model拼接条件执行删除操作.
	**************************************************************************
	-->
    <delete id="deleteBy"
            parameterType="${cfg.poPackage}.${entity}">
        DELETE
        FROM   ${table.name}
        <where>
        <#list table.fields as field>
            <if test="${field.propertyName} != null <#if field.propertyType == "String">${field.name} != ''</#if>">
            <#if field_index==0>
                ${field.name}= ${'#'}{${field.propertyName}}
            <#else>
                AND ${field.name}= ${'#'}{${field.propertyName}}
            </#if>
            </if>
        </#list>
        </where>
    </delete>

    <!--
	**************************************************************************
	* updateById根据ID更新一条数据
	* 注: 根据主键ID执行变更操作.
	**************************************************************************
	-->
    <update id="updateById"
            parameterType="${cfg.poPackage}.${entity}">
        UPDATE
            ${table.name}
        <set>
        <#list table.fields as field>
            <if test="${field.propertyName} != null <#if field.propertyType == "String">${field.name} != ''</#if>">
            <#if field_index==0>
                ${field.name}= ${'#'}{${field.propertyName}}
            <#else>
                ,${field.name}= ${'#'}{${field.propertyName}}
            </#if>
            </if>
        </#list>
        </set>
        WHERE    ${table.fields[0].name} =${'#'}{${table.fields[0].propertyName}}
    </update>

    <!--
    **************************************************************************
    * getModelById根据ID获取一个Model实体
    * 注: 根据主键ID获取一个Model实体.
    **************************************************************************
    -->
    <select id="getModelById"
            parameterType="long"
            resultType="${cfg.poPackage}.${entity}">
        SELECT
            <#list table.fields as field>
                <#if field_index==0>
             t.${field.name}    ${"AS ${field.propertyName}"?left_pad(20)}
                <#else>
            ,t.${field.name}    ${"AS ${field.propertyName}"?left_pad(20)}
                </#if>
            </#list>
        FROM ${table.name} AS t
        WHERE    ${table.fields[0].name} =${'#'}{${table.fields[0].propertyName}}
    </select>

    <!--
	**************************************************************************
	* getModelBy根据Model获取一个Model实体
	* 注: 根据Model实体获取一个Model实体.
	**************************************************************************
	-->
    <select id="getModelBy"
            parameterType="${cfg.poPackage}.${entity}"
            resultType="${cfg.poPackage}.${entity}">
        SELECT
        <#list table.fields as field>
            <#if field_index==0>
             t.${field.name}    ${"AS ${field.propertyName}"?left_pad(20)}
            <#else>
            ,t.${field.name}    ${"AS ${field.propertyName}"?left_pad(20)}
            </#if>
        </#list>
        FROM ${table.name} AS t
        <where>

        <#list table.fields as field>
            <if test="${field.propertyName} != null <#if field.propertyType == "String">${field.name} != ''</#if>">
            <#if field_index==0>
                ${field.name}= ${'#'}{${field.propertyName}}
            <#else>
                AND ${field.name}= ${'#'}{${field.propertyName}}
            </#if>
            </if>

        </#list>
        </where>
    </select>

    <!--
	**************************************************************************
	* getList条件查询，不分页
	* 注: 支持多条件查询、模糊查询、日期比较查询等操作.
	**************************************************************************
	-->
    <select id="getList"
            parameterType="${cfg.poPackage}.${entity}"
            resultType="${cfg.poPackage}.${entity}">
        SELECT
        <#list table.fields as field>
            <#if field_index==0>
             t.${field.name}    ${"AS ${field.propertyName}"?left_pad(20)}
            <#else>
            ,t.${field.name}    ${"AS ${field.propertyName}"?left_pad(20)}
            </#if>
        </#list>
        FROM ${table.name} AS t
        <where>

        <#list table.fields as field>
            <if test="${field.propertyName} != null <#if field.propertyType == "String">${field.name} != ''</#if>">
            <#if field_index==0>
                ${field.name}= ${'#'}{${field.propertyName}}
            <#else>
                AND ${field.name}= ${'#'}{${field.propertyName}}
            </#if>
            </if>

        </#list>
        </where>
    </select>

    <!--
	**************************************************************************
	* getListPage条件查询
	* 注: 支持多条件查询、模糊查询、日期比较查询等操作.
	**************************************************************************
	-->
    <select id="getListPage"
            parameterType="${cfg.poPackage}.${entity}"
            resultType="${cfg.poPackage}.${entity}">
        SELECT
        <#list table.fields as field>
        <#if field_index==0>
             t.${field.name}    ${"AS ${field.propertyName}"?left_pad(20)}
        <#else>
            ,t.${field.name}    ${"AS ${field.propertyName}"?left_pad(20)}
        </#if>
        </#list>
        FROM ${table.name} AS t
        <where>

        <#list table.fields as field>
            <if test="${field.propertyName} != null <#if field.propertyType == "String">${field.name} != ''</#if>">
            <#if field_index==0>
                ${field.name}= ${'#'}{${field.propertyName}}
            <#else>
                AND ${field.name}= ${'#'}{${field.propertyName}}
            </#if>
            </if>

        </#list>
        </where>
    </select>
</mapper>
